<?php include('header.php') ?>
<?php include('messages.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">View Cash In-Out</h3>
		</div>
	</div>
	<div class="row">
      <div class="col-md-12">
         <ol class="breadcrumb">
            <li><a href="<?php echo base_url('manager/');?>">Dash Board</a> </li>
            <li class="active">View Cash In-Out</li>
         </ol>
      </div>
   	</div>
   	<?php include('messages.php');?>
   	<?php echo form_open('manager/storeCashInOut')?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
							    <label>Transaction Type</label>
							    <input type="text" class="form-control" name="transectionType" value="" disabled>
							  </div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
							    <label>Amount</label>
							    <input type="text" class="form-control" name="transectionTotalAmount" value="" disabled >
							  </div>
						</div>				
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
							    <label>Note</label>
							    <textarea type="text" class="form-control" name="transectionDetails" value="" rows="3" disabled></textarea>
							  </div>
						</div>			
					</div>	
					<div class="row">
						<div class="col-md-12">
							<button class="btn btn-danger" onclick="window.history.back();"><i class="fa fa-arrow-left"></i> Back</button>
						</div>			
					</div>				
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close(); ?>
<?php include('footer.php') ?>