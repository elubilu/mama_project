<?php include('header.php') ?>
<?php include('messages.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">View Return-Exchange Details</h3>
		</div>
	</div>
	<div class="row">
      <div class="col-md-12">
         <ol class="breadcrumb">
            <li><a href="<?php echo base_url('manager/');?>">Dash Board</a> </li>
            <li class="active">View Return-Exchange Details</li>
         </ol>
      </div>
   	</div>
	<?php echo form_open('manager/storeReturnProduct'); ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-body">
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
							    <label>Client Name</label>
								<input type="text" class="form-control" name="client_info_saleClientID" value="" disabled>
							</div>						
						</div>	
						<div class="col-md-6">
							<div class="form-group">
								<label>Main Invoice</label>
								<input type="text" class="form-control" name="exchangeInvoiceId" value="" disabled>
							</div>				
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h4 class="">Returned Product</h4>
				</div>
				<div class="panel-body">						
					<div class="row">
						<div class="col-md-12 m-top-15">
							<table class="table table-striped">
								<thead>
									<tr class="active">
										<th>Product Name</th>
										<th>Quantity</th>
										<th>Price</th>
										<th>Amount</th>
									</tr>
								</thead>
								<tbody id="returnProductTable">
																		
								</tbody>
							</table>
						</div>
					</div>						
				</div>	
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h4 class="">Exchanged Product</h4>
				</div>
				<div class="panel-body">
						
					<div class="row">
						<div class="col-md-12 m-top-15">
							<table class="table table-striped">
								<thead>
									<tr class="active">
										<th>Product Name</th>
										<th>Quantity</th>
										<th>Price</th>
										<th>Amount</th>
									</tr>
								</thead>
								<tbody id="exchangeProductTable">		
								</tbody>
							</table>
						</div>
					</div>						
				</div>	
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
							    <label>Total Return Value</label>
							    <input type="text" class="form-control" name="" id="totalReturnSubtotalID" value="0" disabled>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
							    <label>Total Exchange Value</label>
							    <input type="text" class="form-control" name="" id="totalExchangeSubtotalID" value="0" disabled>
							</div>
						</div>	
						<div class="col-md-4">
							<div class="form-group">
							    <label>Adjusted Value</label>
							    <input type="text" class="form-control adjustValueID" name="" value="0" disabled>
								<input type="hidden" class="form-control adjustValueID" name="saleTotalAmount"  value="0" >
							</div>
						</div>				
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="fprm-group">
								<label for="">Return Option</label><br>
								<label class="radio-inline">
								  <input type="radio" name="paymentType" id="inlineRadio1" value="3"> Add to wallet
								</label>
								<label class="radio-inline">
								  <input type="radio" name="paymentType" id="inlineRadio2" value="5"> Return Amount (Cash/Cheque)
								</label>
								<label class="radio-inline">
								  <input type="radio" name="paymentType" id="inlineRadio3" value="4"> Exchange
								</label>
							</div>
						</div>	
					</div>
					<div class="row m-top-15">
						<div class="col-md-12">
							<button class="btn btn-danger" onclick="window.history.back();"><i class="fa fa-arrow-left"></i> Back</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close(); ?>	
<?php include('footer.php') ?>