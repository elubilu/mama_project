<?php include('header.php') ?>
<?php include('messages.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Transaction Report</h3>
		</div>
	</div>
	<div class="row">
      <div class="col-md-12">
         <ol class="breadcrumb">
            <li><a href="<?php echo base_url('manager/');?>">Dash Board</a> </li>
            <li class="active">Transaction Report</li>
         </ol>
      </div>
   	</div>

   	<?php echo form_open('manager/transactionReport'); ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
							    <label>Date From</label>
							    <input type="text" class="form-control" name="fromDate" value="<?php echo date('m/d/Y',strtotime($dates['fromDate'])); ?>" id="startDate">
							 </div>							  
						</div>
						<div class="col-md-4">
							<div class="form-group">
							    <label>Date To</label>
							    <input type="text" class="form-control" name="toDate" value="<?php echo date('m/d/Y',strtotime($dates['toDate']));?>" id="endDate">
							  </div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
							    <label>Type</label>
							    <select name="transectionType" id="" class="form-control forselect2">
									<option value="0">All Transaction Type</option>
									<?php foreach($data as $info) { ?>
									<option value="<?php echo $info->transactionTypeId; ?>"><?php echo $info->transectionTypeName; ?></option>
								   <?php } ?>
								</select>
							  </div>
						</div>				
					</div>
					<div class="row">
						<div class="col-md-12">
							<div  class="form-group">
							    <button type="submit" class="btn btn-primary"><i class="fa fa-thumbs-up"></i> Done</button>
							 </div>							  
						</div>			
					</div>					
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close() ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info filterable">
				<div class="panel-heading">
					<div class="row">
						<div class="col-md-6">
							<h4>Transactions</h4>
						</div>
						<div class="col-md-6">
							<div class="pull-right">
								<button id="filter_button" class="btn btn-warning btn-filter with_print m-top-20" ><i class="fa fa-filter"></i> Filter
								</button>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body">						
					<div class="row">
						<div class="col-md-12 m-top-15">
							<table class="table table-striped">
								<thead>
									<tr class="active filters">
										<th>
											<input type="text" class="form-control" placeholder="Date & Time" disabled data-toggle="true" id="">
										</th>
										<th>
											<input type="text" class="form-control" placeholder="Bill / Invoice / ID" disabled data-toggle="true" id="">
										</th>
										<th>
											<input type="text" class="form-control" placeholder="Details" disabled id="">
										</th>
										<th>
											<input type="text" class="form-control" placeholder="Type" disabled id="">
										</th>
										<th>
											<input type="text" class="form-control" placeholder="Amount" disabled id="">
										</th>
										<th><span>View</span></th>
									</tr>
								</thead>
								<tbody>
								<?php if($infos){ foreach($infos as $value) { ?>
									<tr>
										<td><?php echo date('M-d, Y  h:i a',strtotime($value->transectionDate)) ;  ?></td>
										<td><?php echo $value->transectionReferenceID;  ?></td>
										<td><?php echo $value->transectionDetails;  ?>  </td>
										<td><?php echo $value->transectionTypeName;  ?>  </td>
										<td><?php echo $value->transectionTotalAmount ?></td>
										<td>
											<a href="<?php echo base_url("manager/{$value->transectionTypeLink}/{$value->transectionReferenceID}")?>" class="btn btn-primary btn-sm"><i class="fa fa-info"></i></a>
										</td>
									</tr>	
									<?php } } else{ ?>
									<tr>
										<td>No Data Found! </td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>						
				</div>	
			</div>
		</div>
	</div>
<?php include('footer.php') ?>