<?php include('header.php') ?>
<?php include('messages.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">View Sale Details</h3>
		</div>
	</div>
	<div class="row">
      <div class="col-md-12">
         <ol class="breadcrumb">
            <li><a href="<?php echo base_url('manager/');?>">Dash Board</a> </li>
            <li class="active">View Sale Details</li>
         </ol>
      </div>
   	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
							    <label>Client Name</label>
								<input type="text" id="clientIdForInvoice" class="form-control" name="client_info_saleClientID" value="<?php echo $info->clientContactName; ?>" disabled>
							</div>
						</div>	
						<div class="col-md-6">
							<div class="form-group">
							    <label>Client Contact No.</label>
							    <input type="text" id="clientContactNumberID" class="form-control" name="" value="<?php echo $info->clientContactNumber; ?>" disabled>
							</div>
						</div>	
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
							    <label>Client Address</label>
							    <textarea type="text" id="clientAddressID" class="form-control input-lg" name="" value="<?php echo $info->clientAddress; ?>" disabled><?php echo $info->clientAddress; ?> </textarea>
							  </div>
						</div>
					</div>	
					<div class="row">
						<div class="col-md-12 m-top-15">
							<table class="table table-striped" >
								<thead>
									<tr class="active">
										<th>Product Name</th>
										<th>Quantity</th>
										<th>Sale Price</th>
										<th>Sub-Total</th>
									</tr>
								</thead>
								<?php 
									$totalsubtotal=0; 
								?>
								<tbody id="productTable">
									<?php if($details){ ?>
									<?php foreach($details as $detail): ?>
										<tr id="row">
											<td ><?php echo $detail->productName; ?></td>
											<td><?php echo $detail->saleProductQuantity ;?></td>
											<td><?php echo $detail->salePrice; ?></td>
											<td><?php echo $detail->productSubTotal; ?></td>
										</tr>	
									<?php 
										$totalsubtotal=$totalsubtotal+$detail->productSubTotal;
									?>
									<?php endforeach ; ?>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>	
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
							    <label>Transport Cost</label>
							    <input type="text" class="form-control otherCosts" name="transportCost" id="transportCostID" value="<?php echo $info->transportCost; ?>" disabled>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
							    <label>Additional Cost</label>
							    <input type="text" class="form-control otherCosts" name="additionalCost" id="additionalCostID" value="<?php echo $info->additionalCost; ?>" disabled>
							  </div>
						</div>	
						<div class="col-md-3">
							<div class="form-group">
							    <label>Discount</label>
							    <input type="text" class="form-control otherCosts" name="saleTotalDiscount" id="discountID" value="<?php echo $info->saleTotalDiscount; ?>" disabled>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
							    <label>Paid Amount </label>
							    <input type="text" class="form-control otherCosts" name="receivedTotal" value="<?php echo $info->receivedTotal; ?>" id="paidAmountID" disabled>
							</div>
						</div>	
					</div>
				</div>	
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-4 col-md-offset-8">
					<?php $grandTotal=$info->saleTotalAmount+$info->clientBalance*(-1); ?>
					<ul class="list-group">
					  <li class="list-group-item">Subtotal<span class="pull-right totalSubtotalClass"><?php echo $totalsubtotal; ?></span></li>
					  <li class="list-group-item">Total Bill <span class="pull-right" id="totalBillID"><?php echo $info->saleTotalAmount; ?></span></li>
					  <!--<li class="list-group-item" >Previous Due <span class="pull-right" id="clientBalanceID"><?php //echo $info->clientBalance*(-1); ?></span></li>
					  <li class="list-group-item">Grand Total <span class="pull-right" id="grandTotalID"><?php //echo $grandTotal; ?></span></li>
					  <li class="list-group-item">Total Due <span class="pull-right" id="totalDueID"><?php //echo $grandTotal-$info->receivedTotal; ?></span></li>-->
					</ul>
					<button class="btn btn-danger btn-block pull-right" onclick="window.history.back();"><i class="fa fa-arrow-left"></i> Back</button>
				</div>
			</div>
		</div>
	</div>
<?php include('footer.php') ?>