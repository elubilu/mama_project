<?php include('header.php') ?>
<?php include('messages.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Income-Expense Report</h3>
		</div>
	</div>
	<div class="row">
      <div class="col-md-12">
         <ol class="breadcrumb">
            <li><a href="<?php echo base_url('manager/');?>">Dash Board</a> </li>
            <li class="active">Income-Expense Report</li>
         </ol>
      </div>
   	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-body">
					<?php echo form_open('manager/incomeExpenseReport'); ?>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
							    <label>Date From</label>
							    <input type="text" class="form-control" name="fromDate" value=" <?php echo date('m/d/Y',strtotime($dates['fromDate'])); ?>" id="startDate">
							 </div>							  
						</div>
						<div class="col-md-4">
							<div class="form-group">
							    <label>Date To</label>
							    <input type="text" class="form-control" name="toDate" value="<?php echo date('m/d/Y',strtotime($dates['toDate']));?>" id="endDate">
							  </div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
							    <label>Select By Transaction Type</label>
							    <select name="transectionType" id="" class="form-control forselect2">
									<option value="0">Select an Option</option>
									<option value="1">Product Sale</option>
									<option value="8">Quick Sale</option>
									<option value="3">Product Exchange</option>
									<option value="2">Expense</option>
								</select>
							</div>
						</div>				
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
							    <button type="submit" class="btn btn-primary"><i class="fa fa-thumbs-up"></i> Done</button>
							 </div>							  
						</div>			
					</div>
					<?php echo form_close(); ?>					
				</div>
			</div>
		</div>
	</div>
	<?php $totalIncome=0; $totalExpense=0; ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info filterable">
				<div class="panel-heading">
					<div class="row">
						<div class="col-md-12">
							<div class="pull-right">
								<button id="filter_button" class="btn btn-warning btn-filter with_print m-top-20" ><i class="fa fa-filter"></i> Filter
								</button>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body">						
					<div class="row">
						<div class="col-md-12 m-top-15">
							<table class="table table-striped">
								<thead>
									<tr class="active filters">
										<th>
											<input type="text" class="form-control" placeholder="Date" disabled data-toggle="true" id="">
										</th>
										<th>
											<input type="text" class="form-control" placeholder="Type" disabled id="">
										</th>
										<th>
											<input type="text" class="form-control" placeholder="Details" disabled id="">
										</th>
										<th>
											<input type="text" class="form-control" placeholder="Amount" disabled id="">
										</th>
										<th><span>View</span></th>
									</tr>
								</thead>
								<tbody>
									<?php if($infos){ foreach($infos as $info): ?>
										<tr>
											<td><?php echo date('m-d Y h:ia',strtotime($info->transectionDate)); ?></td>
											<td><?php echo $info->transectionTypeName; ?></td>
											<td><?php echo $info->transectionDetails; ?></td>
											<td><?php echo $info->transectionTotalAmount; ?></td>
											<td>
												<a href="<?php echo base_url("manager/{$info->transectionTypeLink}/{$info->transectionReferenceID}")?>" class="btn btn-primary btn-sm"><i class="fa fa-info"></i></a>
											</td>
										</tr>
									<?php 
										if($info->transectionType==2)
											$totalExpense=$totalExpense+$info->transectionTotalAmount; 
										else 
											$totalIncome=$totalIncome+$info->transectionTotalAmount; 
									endforeach; }
									else{  ?>
									<tr><td>No Data Found!</td></tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>						
				</div>	
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
							    <label>Total Income</label>
							    <input type="text" class="form-control" name="" value="<?php echo $totalIncome; ?>" id="startDate">
							 </div>							  
						</div>
						<div class="col-md-6">
							<div class="form-group">
							    <label>Total Expense</label>
							    <input type="text" class="form-control" name="" value="<?php echo $totalExpense; ?>" id="endDate">
							  </div>
						</div>				
					</div>					
				</div>
			</div>
		</div>
	</div>
<?php include('footer.php') ?>