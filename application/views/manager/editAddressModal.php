<div class="modal fade" id="editAddressModal<?php echo $address->clientAddressInfoId; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Edit Address</h4>
			</div>
			<?php echo form_open('manager/editAddressModal'); ?>
			<?php echo form_hidden('clientAddressInfoId',$address->clientAddressInfoId); ?>
			<?php echo form_hidden('id',$data->clientID); ?>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="">Title</label>
							<input type="text" class="form-control" name="clientAddressTitle" value="<?php echo $address->clientAddressTitle; ?>" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="">Contact No</label>
							<input type="text" class="form-control"  name="clientAddressContact" value="<?php echo $address->clientAddressContact; ?>"  />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="">Address</label>
							<textarea type="text" class="form-control" name="clientAddress" ><?php echo $address->clientAddress; ?> </textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Save</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>