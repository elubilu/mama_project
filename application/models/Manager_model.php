﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manager_model extends CI_Model {
	/* Total Cash */
	public function get_total_cash_amount()
	{
		$cash=0;
		$expense=0;
		$income =$this->db->select_sum('transectionTotalAmount')
				 ->from('transection_info')
				 ->where("(transectionType = '1' OR transectionType = '3' OR transectionType = '4' OR transectionType = '8' OR transectionType = '9' OR transectionType = '11') ")
		         ->get();
		$cash= $income->row()->transectionTotalAmount;
		
		$q =$this->db->select_sum('transectionTotalAmount')
				 ->from('transection_info')
				 ->where("(transectionType = '5' OR transectionType = '10' OR transectionType = '12') ")
		         ->get();
		$expense=$q->row()->transectionTotalAmount;
		$totalAmount=$cash-$expense;
		return $totalAmount;
		
	}
	/* Start My Info*/
	
	public function my_info($user_id){
        $q=$this->db
            ->select('*')
            ->from('admin_info')
            ->where('adminID',$user_id)
            ->where('adminStatus', 1)
            ->get();
        if($q->num_rows()==1){
			return $q->row();
		}
        else{
            return FALSE;
        }
    }
	public function get_password_by_id($id){
		$data=$this->db
				->select('adminPassword')
				->from('admin_info')
				->where('adminID',$id)
				->get();

		if($data)
		{
			return $data->row()->adminPassword;
		}
	}

	public function update_contact($admin_id,$data){
		return $this->db
			        ->where('adminID',$admin_id)
				    ->update('admin_info',$data);
	}
	
	public function change_password($data,$admin_id){
		return $this->db
			        ->where('adminID',$admin_id)
				    ->update('admin_info',$data);
	}
	/* End My Info*/
	/* Start User Info*/
	public function add_user($data)
	{
		unset($data['confirmPass']);
		return $this->db->insert('admin_info', $data );
	}
	public function get_all_user()
	{
		
		$q=$this->db
				->select('admin_info.adminID,admin_info.adminUserID,admin_info.adminName,admin_role.roleName')
				->from('admin_info')
				->join('admin_role','admin_role.roleID=admin_info.admin_role_roleID')
				->where('admin_role_roleID>',1)
				//->limit($limit,$offset)
				->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_user_details_by_id($admin_id)
	{
		
		$q=$this->db
				->select('*,admin_role.roleName')
				->from('admin_info')
				->join('admin_role','admin_role.roleID=admin_info.admin_role_roleID')
				->where('adminID',$admin_id)
				->where('admin_role_roleID>',1)
				//->limit($limit,$offset)
				->get();
				
		if($q->num_rows()==1){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	
	public function update_user($data,$admin_id)
	{
		unset($data['adminID']);
		return $this->db
					->where('adminID',$admin_id)
					->update('admin_info',$data);
		
	}
	/* End User Info*/
	
	/* Start Clients Info*/
	
	public function addClient($data){
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
        $date= $dt->format('Y-m-d H:i:s');
        $data['clientAddedDate']=$date;
		$data['businessStartedSince']=date('Y-m-d', strtotime($data['businessStartedSince']));
        $data['clientAddedBy']=$this->session->userdata('admin_id');
		
		$address['clientAddressTitle']=$data['addressTitle'];
		$address['clientAddress']=$data['address'];
		$address['clientAddressContact']=$data['clientContactNumber'];
		$address['clientAddressType']=1;
		unset($data['addressTitle'], $data['address']);
		if($this->db->insert('client_info', $data )){
			$address['client_info_clientID']=$this->db->insert_id();
			return $this->db->insert('client_address_info', $address);
		}
	}

	public function get_all_clients(){
		$data = $this->db
					->select('*')
					->from('client_info')
					->where('clientType',1)
					->get();

		if($data){
			return $data->result();
		}
	}

	public function get_all_client_name_id(){
		$data = $this->db
					->select('clientID,clientContactName,clientContactNumber')
					->from('client_info')
					->where('clientType',1)
					->where('clientStatus',1)
					->get();

		if($data){
			return $data->result();
		}
	}
	public function get_all_type_clients_id_name(){
		$data = $this->db
					->select('clientID,clientContactName,clientType')
					->from('client_info')
					->where('clientStatus',1)
					->get();

		if($data){
			return $data->result();
		}
	}
	public function ajax_get_client_contact_by_id($id){
		$data=$this->db
				->select('client_info.clientContactNumber,client_info.clientBalance,client_address_info.clientAddress')
				->from('client_info')
				->join('client_address_info','client_info.clientID=client_address_info.client_info_clientID')
				->where('clientID',$id)
				->where('clientAddressType',1)
				->where('clientAddressStatus',1)
				->get();

		if($data)
		{
			return $data->row();
		}
	}
	public function ajax_get_client_last_five_transaction($id){
		$data=$this->db->select('transectionTotalAmount,transectionDetails,transectionDate,transection_type_info.transectionTypeName')
				->from('transection_info')
				->join('transection_type_info','transection_info.transectionType=transection_type_info.transactionTypeId')
				->where('client_info_transectionClientId',$id)
				->order_by('transectionDate','desc')
				->limit(5)
				->get();

		if($data)
		{
			return $data->result();
		}
	}
	public function get_client_details_by_ID($id){
		$data=$this->db
				->select('*')
				->from('client_info')
				->where('clientID',$id)
				->get();

		if($data)
		{
			return $data->row();
		}
	}
	public function get_client_address_details_by_ID($id){
		$data=$this->db
				->select('*')
				->from('client_address_info')
				->where('client_info_clientID',$id)
				->where('clientAddressStatus',1)
				->limit(3)
				->order_by('clientAddressType','asc')
				->get();

		if($data)
		{
			return $data->result();
		}
	}
	public function get_client_transaction_details_by_ID($id){
		$data=$this->db
				->select('*')
				->from('transection_info')
				->join('transection_type_info','transection_info.transectionType=transection_type_info.transactionTypeId')
				->where('client_info_transectionClientId',$id)
				->order_by('transectionDate','desc')
				->get();

		if($data)
		{
			return $data->result();
		}
	}

	public function update_client_info($data){
		$id=$data['id'];
		unset($data['id']);

		$data=$this->db
			->where('clientID',$id)
			->update('client_info',$data);
		return $data;
	}
	public function update_admin_info($data){
		$id=$data['adminID'];
		unset($data['adminID']);
		if($data['adminPasswordOld']){
			unset($data['adminPasswordOld']);
			$data['adminPassword']=$data['adminPasswordNew'];
			unset($data['adminPasswordNew']);
			unset($data['adminPasswordConfirm']);
		}
		else
		{
			unset($data['adminPasswordNew']);
			unset($data['adminPasswordConfirm']);
			unset($data['adminPasswordOld']);
		}

		$data=$this->db
			->where('adminID',$id)
			->update('admin_info',$data);
		return $data;
	}

	public function get_all_suppliers(){
		$data = $this->db
					->select('*')
					->from('client_info')
					->where('clientType',2)
					->get();

		if($data){
			return $data->result();
		}
	}
	public function add_client_address($data)
	{
		return $this->db->insert('client_address_info', $data);
	}

   public function remove_address($id)
   {
   	  $this->db->where('clientAddressInfoId',$id);
   	  $this->db->delete('client_address_info');
   	 
   	 			
   	 			
   	 
   }
	/* End Clients Info*/
	
	/* Start product Info*/
	
	public function store_product_type($data){
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
        $date= $dt->format('Y-m-d H:i:s');
        $data['productTypeAddedDate']=$date;
		return $this->db->insert('product_type_info', $data);
	}
	public function all_product_type()
	{
		$q=$this->db
				->select('product_type_info.productTypeID, product_type_info.productTypeName')
				->from('product_type_info')
				->where('productTypeStatus', 1)
				->get();
				
		if($q->num_rows() > 0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function store_product_to_inventory($data){
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
        $date= $dt->format('Y-m-d H:i:s');
        $data['productAddedDate']=$date;
		return $this->db->insert('product_info', $data);
	}
	
	public function get_all_active_product_id_name(){
		$q=$this->db->select('productId,productName')
					->from('product_info')
					->where('productStatus',1)
					->get();
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_products_details_by_id($id)
	{
		$data = $this->db
					->select('*')
					->from('product_info')
					->where('productId',$id)
					->get();

		if($data){
			return $data->row();
		}
	}
	public function update_product_info($data){
		return $this->db
			        ->where('productId',$data['productId'])
				    ->update('product_info',$data);
	}
	public function ajax_get_product_info_search($keyword,$strup,$strlow,$strupper,$strfirst){
		$query =$this->db->select('product_info.productName,product_info.productId')
						 ->from('product_info') 
						 ->where('productStatus',1) 
						 ->group_start()
						 ->like('productName',$keyword,'both') 
						 ->or_like('productName',$strup,'both') 
						 ->or_like('productName',$strupper,'both') 
						 ->or_like('productName',$strlow,'both') 
						 ->or_like('productName',$strfirst,'both') 
						 ->group_end()
						 ->get();     
        return $query->result(); 
	}
	public function ajax_get_product_quantity_by_id($id){
		$q=$this->db->select('productQuantity,productPurchasePrice,productSalePrice')
					->from('product_info')
					->where('productid',$id)
					->where('productStatus',1)
					->get();
		if($q->num_rows()){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	public function ajax_get_inventory_product_serach_info($data){
		$this->db->select('productId,productName,productQuantity,productTypeName')
				->from('product_info')
				->join('product_type_info','product_info.product_type_info_productTypeID=product_type_info.productTypeID');
		if($data['product_id']>0){
			$this->db->where('productId',$data['product_id']);
		}
		if($data['type_id']>0){
			$this->db->where('product_type_info_productTypeID',$data['type_id']);
		}
		if($data['client_id']>0){
			$this->db->where('supplier_info_productSupplierID',$data['client_id']);
		}
		$q=$this->db->get();
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function add_product_to_stock($data,$product_id,$quantity)
	{
		$this->db->trans_start();
		$data['productUpdatedDate']=date('Y-m-d H:i:s', strtotime($data['productUpdatedDate']));
		$data['productUpdatedBy']=$this->session->userdata('admin_id');
		$q=$this->db
				->where('productId',$product_id)
				->set('productQuantity', 'productquantity + ' . $quantity, FALSE)
				->update('product_info',$data);
		if($q){
			$update['productUpdatedQuantity']=$quantity;
			$update['product_info_updatedProductID']=$product_id;
			//$update['productUpdateDate']=$data['productUpdatedDate'];
			$update['admin_info_productUpdatedBy']=$this->session->userdata('admin_id');
			$this->db->insert('product_update_info', $update );
		}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE){
			return false;
		}		
		
		else return true;
		
	}

	public function get_all_productType(){
		$data = $this->db
					->select('*')
					->from('product_type_info')
					//->where('productTypeStatus',1)
					->get();

		if($data){
			return $data->result();
		}
	}
	public function get_productType_details_by_ID($id){
		$data=$this->db
				->select('*')
				->from('product_type_info')
				->where('productTypeID',$id)
				->get();

		if($data)
		{
			return $data->row();
		}
	}	
	
	public function update_view_product_type($data){
		$data=$this->db
				->where('productTypeID',$data['productTypeID'])
				->update('product_type_info',$data);
			return $data;
	}
	
	public function ajax_get_product_details_by_id($product_id)
	{
		$q=$this->db
				->select('product_info.*,')
				->from('product_info')
				->where('productId',$product_id)
				->get();
				
		if($q->num_rows()==1){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	
	/* End product Info*/
	/*Start Sale*/
	
	public function store_invoice($data,$cart)
	{
		$this->db->trans_start();
		$data['saleTotalAmount']=($data['productSubtotal']+$data['transportCost']+$data['additionalCost'])-$data['saleTotalDiscount'] ;
		$data['entryBy']=$this->session->userdata('admin_id');
		$dueAmount=$data['receivedTotal']-$data['saleTotalAmount'];
		unset($data['productSubtotal']);
		$this->db->insert('sale_info', $data );
		$error = $this->db->error();
		if($error['code']!=0){
			return false;
			
		}
		$sale_id= $this->db->insert_id();
		foreach ($cart as $product) {
            $products=array(
                'product_info_saleProductID'=>$product['id'],
                'saleProductQuantity'=>$product['qty'],
                'salePrice'=>$product['salePrice'],
				'mainPrice'=>$product['price'],
                'sale_info_saleID'=>$sale_id,
                'productSubTotal'=>$product['salePrice']*$product['qty'],

            );
			$this->db
			    ->where('productID',$products['product_info_saleProductID'])
				->set('productQuantity', 'productQuantity - ' . (int) $products['saleProductQuantity'], FALSE)
				->set('productSaleCounter', 'productSaleCounter + ' .(int) $products['saleProductQuantity'], FALSE)
				->update('product_info');
            $this->db->insert('sale_details', $products );
        }
		$transection['transectionType']=1;
		$transection['transectionReferenceID']=$sale_id;
		$transection['transectionDetails']="Sale";
		$transection['transectionBy']=$data['entryBy'];
		$transection['transectionDate']=$data['saleDate'];
		$transection['transectionTotalAmount']=$data['receivedTotal'];
		$transection['client_info_transectionClientId']=$data['client_info_saleClientID'];
		$this->db->insert('transection_info', $transection );
		$this->db
			 ->where('clientID',$data['client_info_saleClientID'])
			 ->set('clientBalance', 'clientBalance + ' . $dueAmount, FALSE)
			 ->update('client_info');

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE){
			$sale_id = false;
		}		
		if($sale_id)
		{
			return $sale_id;
		}
		else return false;
	}
	
	public function store_quick_sale($data,$cart)
	{
		$this->db->trans_start();
		$data['saleTotalAmount']=($data['productSubtotal']+$data['transportCost']+$data['additionalCost'])-$data['saleTotalDiscount'] ;
		$data['entryBy']=$this->session->userdata('admin_id');
		$data['saleType']=8;
		$dueAmount=$data['receivedTotal']-$data['saleTotalAmount'];
		unset($data['productSubtotal']);
		$this->db->insert('sale_info', $data );
		$error = $this->db->error();
		if($error['code']!=0){
			return false;
			
		}
		$sale_id= $this->db->insert_id();
		foreach ($cart as $product) {
            $products=array(
                'product_info_quickSaleProductID'=>$product['id'],
                'quickSaleProductQuantity'=>$product['qty'],
                'quickSaleSalePrice'=>$product['salePrice'],
				'quickSaleMainPrice'=>$product['price'],
				'quickSaleBuyPrice'=>$product['buy_price'],
                'sale_info_quickSaleID'=>$sale_id,
                'quickSaleProductSubTotal'=>$product['salePrice']*$product['qty'],

            );
			/*$this->db
			    ->where('productID',$products['product_info_quickSaleProductID'])
				->set('productQuantity', 'productQuantity - ' . (int) $products['quickSaleProductQuantity'], FALSE)
				->set('productSaleCounter', 'productSaleCounter + ' .(int) $products['quickSaleProductQuantity'], FALSE)
				->update('product_info');*/
            $this->db->insert('quick_sale_details', $products );
        }
		$transection['transectionType']=8;
		$transection['transectionReferenceID']=$sale_id;
		$transection['transectionDetails']="Quick Sale";
		$transection['transectionBy']=$data['entryBy'];
		$transection['transectionDate']=$data['saleDate'];
		$transection['transectionTotalAmount']=$data['receivedTotal'];
		$transection['client_info_transectionClientId']=$data['client_info_saleClientID'];
		$this->db->insert('transection_info', $transection );
		
		$this->db
			 ->where('clientID',$data['client_info_saleClientID'])
			 ->set('clientBalance', 'clientBalance + ' . $dueAmount, FALSE)
			 ->update('client_info');

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE){
			$sale_id = false;
		}		
		if($sale_id)
		{
			return $sale_id;
		}
		else return false;
	}
	
	/*End Sale*/
	/* Start Return Product */
	
	public function store_return_product($data,$cart)
	{
		$this->db->trans_start();
		$data['entryBy']=$this->session->userdata('admin_id');
		$data['saleType']=3;
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
		$date= $dt->format('Y-m-d H:i:s');
		$data['saleDate']=$date;
		$data['receivedTotal']=$data['saleTotalAmount'];
		$this->db->insert('sale_info', $data );
		$error = $this->db->error();
		if($error['code']!=0){
			return false;
			
		}
		$sale_id= $this->db->insert_id();
		foreach ($cart as $product) {

            $products=array(
                'exchangeProductID'=>$product['id'],
                'exchangeProductQuantity'=>$product['qty'],
                'exchangeProductPrice'=>$product['price'],
                'sale_info_exchangeSaleId'=>$sale_id,
                'exchangeProductSubtotal'=>$product['price']*$product['qty'],
                'exchangeProductType'=>$product['product_type']

            );
			if($product['product_type']==1){
				$this->db
					->where('productID',$product['id'])
					->set('productQuantity', 'productQuantity - ' . (int) $product['qty'], FALSE)
					->set('productSaleCounter', 'productSaleCounter + ' .(int) $product['qty'], FALSE)
					->update('product_info');
					
				$sale['sale_info_saleID']=$sale_id;
				$sale['product_info_saleProductID']=$product['id'];
				$sale['saleProductQuantity']=$product['qty'];
				$sale['salePrice']=$product['price'];
				$sale['mainPrice']=$product['mainPrice'];
				$sale['productSubTotal']=$product['price']*$product['qty'];
				$this->db->insert('sale_details', $sale );
			}
			if($product['product_type']==0){
				$this->db
					->where('productID',$product['id'])
					->set('productQuantity', 'productQuantity + ' . (int) $product['qty'], FALSE)
					->set('productSaleCounter', 'productSaleCounter - ' .(int) $product['qty'], FALSE)
					->update('product_info');
			}
            $this->db->insert('exchange_details', $products );
			
        }
		$transection['transectionType']=$data['saleType'];
		$transection['transectionReferenceID']=$sale_id;
		$transection['transectionDetails']="Quick Sale";
		$transection['transectionBy']=$data['entryBy'];
		$transection['transectionDate']=$data['saleDate'];
		$transection['transectionTotalAmount']=$data['receivedTotal'];
		$transection['client_info_transectionClientId']=$data['client_info_saleClientID'];
		$this->db->insert('transection_info', $transection );
		$dueAmount=$data['saleTotalAmount']*(-1);
		if($data['paymentType']==3){
			$this->db
				 ->where('clientID',$data['client_info_saleClientID'])
				 ->set('clientBalance', 'clientBalance + ' . $dueAmount, FALSE)
				 ->update('client_info');
		}	
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE){
			$sale_id = false;
		}		
		if($sale_id)
		{
			return $sale_id;
		}
		else return false;
	}
	
	
	
	/* End Return Product */
	/* Start Expense */
	public function store_expense_info($data){
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
        $date= $dt->format('Y-m-d H:i:s');
        $data['expenceDate']=$date;
		$data['expenceEntryBy'] = $this->session->userdata('admin_id');
		if($this->db->insert('expense_info', $data )){
			$expense_id=$this->db->insert_id();
			$transection['transectionType']=2;
			$transection['transectionReferenceID']=$expense_id;
			$transection['transectionDetails']=$data['expenseNote'];
			$transection['transectionBy']=$data['expenceEntryBy'];
			$transection['transectionDate']=$data['expenceDate'];
			$transection['transectionTotalAmount']=$data['expenceAmount'];
			return $this->db->insert('transection_info', $transection );
		}
		else return false;
	}
	public function store_expense_field_info($data){
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
        $date= $dt->format('Y-m-d H:i:s');
        $data['expenseFieldAddedDate']=$date;
		//$data['expenceEntryBy'] = $this->session->userdata('admin_id');
		return $this->db->insert('expense_field_info', $data );
		
	}
	public function get_all_expenses(){
		$data = $this->db
					->select('*')
					->from('expense_info')
					->get();

		if($data){
			return $data->result();
		}
	}
	public function get_all_expense_details(){
		$data = $this->db
					->select('*')
					->from('expense_field_info')
					//->where('expenseFieldStatus',1)
					->get();

		if($data){
			return $data->result();
		}
	}
	public function get_all_expense_name_id(){
		$data = $this->db
					->select('*')
					->from('expense_field_info')
					->where('expenseFieldStatus',1)
					->get();

		if($data){
			return $data->result();
		}
	}
	public function get_expence_info_by_id($id)
	{

		
		$data=$this->db
				->select('*')
				->from('expense_info')
				->where('expenseID',$id)
				->get();
		//print_r($data); exit();
			//->join('supplier_info','product_info.supplier_info_productSupplierID=supplier_info.supplierID')	
		if($data){
			return $data->row();
		}
	}
	public function get_expence_field_info_by_id($id)
	{

		
		$data=$this->db
				->select('*')
				->from('expense_field_info')
				->where('expenseFieldID',$id)
				->get();
		//print_r($data); exit();
			//->join('supplier_info','product_info.supplier_info_productSupplierID=supplier_info.supplierID')	
		if($data){
			return $data->row();
		}
	}
	public function update_expense_field($data){
		return $this->db
			        ->where('expenseFieldID',$data['expenseFieldID'])
				    ->update('expense_field_info',$data);
	}
	
	/* End Expense */
	/* Start Cash Transfer */
	
	public function get_all_cash_transfers(){
		$data = $this->db
					->select('cash_transfer_details.*,from.cashAccountName as fromAccountName,to.cashAccountName as toAccountName')
					->from('cash_transfer_details')
					//->join('cash_account_info','cash_account_info.cashAccountInfoId=cash_transfer_details.cashTransferFrom AND cash_account_info.cashAccountInfoId=cash_transfer_details.cashTransferTo')
					->join('cash_account_info as from','from.cashAccountInfoId=cash_transfer_details.cashTransferFrom',"LEFT")
					->join('cash_account_info as to','to.cashAccountInfoId=cash_transfer_details.cashTransferTo',"LEFT")
					->order_by('cashtransferDate','desc')
					->get();

		if($data){
			return $data->result();
		}
	}
	public function get_transfer_details_by_id($id){
		$data = $this->db
					->select('cash_transfer_details.*,from.cashAccountName as fromAccountName,to.cashAccountName as toAccountName')
					->from('cash_transfer_details')
					->join('cash_account_info as from','from.cashAccountInfoId=cash_transfer_details.cashTransferFrom',"LEFT")
					->join('cash_account_info as to','to.cashAccountInfoId=cash_transfer_details.cashTransferTo',"LEFT")
					->where('cashTransferDetailsId',$id)
					->get();

		if($data){
			return $data->row();
		}
	}
	public function store_cash_transfer($data){
		$this->db->trans_start();
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
        $date= $dt->format('Y-m-d H:i:s');
        $data['cashtransferDate']=$date;
		$data['cashTransferBy'] = $this->session->userdata('admin_id');
		if($this->db->insert('cash_transfer_details', $data )){
			$transfer_id=$this->db->insert_id();
			$cashout=$data['cashTransferFrom'];
			$cashin=$data['cashTransferTo'];
			if($cashout>0){
				$this->db
					->where('cashAccountInfoId',$cashout)
					->set('cashAccountBalance', 'cashAccountBalance - ' . $data['cashTransferAmount'], FALSE)
					->update('cash_account_info');
			}
			else{
				if($cashout==-1){
					$transection['transectionType']=10;
				}
				else if($cashout==0){
					$transection['transectionType']=7;
				}
				$transection['transectionReferenceID']=$transfer_id;
				$transection['transectionDetails']=$data['cashTransferNote'];
				$transection['transectionBy']=$data['cashTransferBy'];
				$transection['transectionDate']=$data['cashtransferDate'];
				$transection['transectionTotalAmount']=$data['cashTransferAmount'];
				$this->db->insert('transection_info', $transection );
			}
			if($cashin>0){
				$this->db
					->where('cashAccountInfoId',$cashin)
					->set('cashAccountBalance', 'cashAccountBalance + ' . $data['cashTransferAmount'], FALSE)
					->update('cash_account_info');
			}
			else{
				if($cashin==-1){
					$transection['transectionType']=9;
				}
				else if($cashin==0){
					$transection['transectionType']=6;
				}
				$transection['transectionReferenceID']=$transfer_id;
				$transection['transectionDetails']=$data['cashTransferNote'];
				$transection['transectionBy']=$data['cashTransferBy'];
				$transection['transectionDate']=$data['cashtransferDate'];
				$transection['transectionTotalAmount']=$data['cashTransferAmount'];
				$this->db->insert('transection_info', $transection );
			}
		}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE){
			return false;
		}		
		
		else return true;
	}
	
	public function get_all_cash_account_name_id(){
		$data = $this->db
					->select('cashAccountInfoId,cashAccountName')
					->from('cash_account_info')
					->where('cashAccountStatus',1)
					->get();

		if($data){
			return $data->result();
		}
	}
	/* End Cash Tranfer */
	/* Start Cash in out */
	public function store_cash_in_out($data){
		//$this->db->trans_start();
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
        $date= $dt->format('Y-m-d H:i:s');
        $data['transectionDate']=$date;
		$data['transectionBy'] = $this->session->userdata('admin_id');
		if($this->db->insert('transection_info', $data)){
			$id=$this->db->insert_id();
			$transection['transectionReferenceID']=$id;
			return $this->db->where('transectionID',$id)
							->update('transection_info',$transection );
		}
		return false;
		
	}
	public function get_transaction_info_by_id($id)
	{
		$q=$this->db->select('*')
				 ->from('transection_info')
				 ->join('transection_type_info','transection_info.transectionType=transection_type_info.transactionTypeId')
				 //->join('client_info','transection_info.client_info_transectionClientId=client_info.clientID')
				 ->where('client_info_transectionClientId',$id)
				 ->order_by('transectionDate','desc')
			     ->get();
				
		if($q){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	/* End Cash in out */
	
	/* Start Petty Cash*/
	public function get_all_petty_cash_info()
	{
		$q=$this->db->select('*')
				 ->from('transection_info')
				 ->join('transection_type_info','transection_info.transectionType=transection_type_info.transactionTypeId')
				 ->where("(transectionType = '2' OR transectionType = '6' OR transectionType = '7') ")
				 ->order_by('transectionDate','desc')
			     ->get();
				
		if($q->num_rows()>0){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_total_petty_cash_amount()
	{
		$cash=0;
		$expense=0;
		$income =$this->db->select_sum('transectionTotalAmount')
				 ->from('transection_info')
				 ->where('transectionType',6)
		         ->get();
		$cash= $income->row()->transectionTotalAmount;
		
		$q =$this->db->select_sum('transectionTotalAmount')
				 ->from('transection_info')
				 ->where("(transectionType = '2' OR transectionType = '7') ")
		         ->get();
		$expense=$q->row()->transectionTotalAmount;
		$totalAmount=$cash-$expense;
		return $totalAmount;
		
	}
	
	/* End Petty Cash*/
	
	
	public function get_sale_info_by_id($sale_id)
	{
		$q=$this->db->select('sale_info.*,client_info.clientContactName,client_info.clientContactNumber,client_info.clientBalance,client_address_info.clientAddress,')
				 ->from('sale_info')
				 ->join('client_info','sale_info.client_info_saleClientID=client_info.clientID')
				 ->join('client_address_info','client_info.clientID=client_address_info.client_info_clientID')
				 ->where('saleID',$sale_id)
				 ->get();
		if($q->num_rows()){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	public function get_sale_info_details_by_id($sale_id)
	{
		$q=$this->db
				//->select('sale_info.*,shop_info.shopTitle,salesman_info.salesmanName')
				->select('sale_details.*,product_info.productName')
				->from('sale_details')
				->join('product_info','sale_details.product_info_saleProductID=product_info.productID')
				->where('sale_info_saleID',$sale_id)
			    ->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_quick_sale_info_details_by_id($sale_id)
	{
		$q=$this->db
				//->select('sale_info.*,shop_info.shopTitle,salesman_info.salesmanName')
				->select('quick_sale_details.*,product_info.productName')
				->from('quick_sale_details')
				->join('product_info','quick_sale_details.product_info_quickSaleProductID=product_info.productID')
				->where('sale_info_quickSaleID',$sale_id)
			    ->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_all_expense_field_id_name()
	{
		$q=$this->db
				->select('expense_field_info.expenseFieldID,expense_field_info.expenseFieldName')
				->from('expense_field_info')
				->where('expenseFieldStatus',1)
				->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function store_expense($data)
	{
		
		if($this->db->insert('expense_info', $data )){
			$expense_id= $this->db->insert_id();
			$transection['transectionType']=2;
			$transection['transectionReferenceID']=$expense_id;
			$transection['transectionDetails']=$data['expenceReference'];
			$transection['transectionBy']=$data['expenceEntryBy'];
			$transection['transectionDate']=$data['expenceDate'];
			$transection['shop_info_transectionShopID']=$data['shop_info_ExpenceShopID'];
			$transection['transectionTotalAmount']=$data['expenceAmount'];
			return $this->db->insert('transection_info', $transection );
		}
		else return false;
	}
	
	
	
	public function get_cash_amount(){
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
		$date= $dt->format('Y-m-d 00:00:00');
		$shop_id=$this->session->userdata('userShop');
		$this->db->select_sum('transectionTotalAmount');
		$this->db->from('transection_info');
		$this->db->where('shop_info_transectionShopID',$shop_id);
		$this->db->where('transectionType',1);
		$this->db->where('transectionDate>=',$date);
		$query = $this->db->get();
		$income= $query->row()->transectionTotalAmount;
		//$shop_id=$this->session->userdata('userShop');
		$this->db->select_sum('transectionTotalAmount');
		$this->db->from('transection_info');
		$this->db->where('shop_info_transectionShopID',$shop_id);
		$this->db->where('transectionType',3);
		$this->db->where('transectionDate>=',$date);
		$query = $this->db->get();
		$return= $query->row()->transectionTotalAmount;
		$income=$income+$return;
		$this->db->select_sum('transectionTotalAmount');
		$this->db->from('transection_info');
		$this->db->where('transectionDate >=',$date);
		$this->db->where('shop_info_transectionShopID',$shop_id);
		$this->db->where('transectionType',2);
		$query = $this->db->get();
		$expense= $query->row()->transectionTotalAmount;
		return $income-$expense;
	}
	
	public function total_income(){
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
		$date= $dt->format('Y-m-d 00:00:00');
		$shop_id=$this->session->userdata('userShop');
		$this->db->select_sum('transectionTotalAmount');
		$this->db->from('transection_info');
		$this->db->where('shop_info_transectionShopID',$shop_id);
		$this->db->where('transectionDate>=',$date);
		$this->db->where('transectionType',1);
		$query = $this->db->get();
		$income= $query->row()->transectionTotalAmount;
		return $income;
	}
	
	public function total_expense(){
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
		$date= $dt->format('Y-m-d 00:00:00');
		$shop_id=$this->session->userdata('userShop');
		$this->db->select_sum('transectionTotalAmount');
		$this->db->from('transection_info');
		$this->db->where('transectionType',2);
		$this->db->where('shop_info_transectionShopID',$shop_id);
		$this->db->where('transectionDate>=',$date);
		$query = $this->db->get();
		$expense= $query->row()->transectionTotalAmount;
		return $expense;
	}
	
	public function total_return(){
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
		$date= $dt->format('Y-m-d 00:00:00');
		$shop_id=$this->session->userdata('userShop');
		$this->db->select_sum('transectionTotalAmount');
		$this->db->from('transection_info');
		$this->db->where('transectionType',3);
		$this->db->where('shop_info_transectionShopID',$shop_id);
		$this->db->where('transectionDate>=',$date);
		$query = $this->db->get();
		$return= $query->row()->transectionTotalAmount;
		return $return;
	}
	public function total_income_by_dates($startDate,$endDate){
		$shop_id=$this->session->userdata('userShop');
		$this->db->select_sum('transectionTotalAmount');
		$this->db->from('transection_info');
		$this->db->where('transectionDate >=',$startDate);
		$this->db->where('transectionDate <=',$endDate);
		$this->db->where('shop_info_transectionShopID',$shop_id);
		$this->db->where('transectionType=1');
		$query = $this->db->get();
		$income= $query->row()->transectionTotalAmount;
		return $income;
	}
	
	public function total_expense_by_dates($startDate,$endDate){
		$shop_id=$this->session->userdata('userShop');
		$this->db->select_sum('transectionTotalAmount');
		$this->db->from('transection_info');
		$this->db->where('transectionDate >=',$startDate);
		$this->db->where('transectionDate <=',$endDate);
		$this->db->where('shop_info_transectionShopID',$shop_id);
		$this->db->where('transectionType=2');
		$query = $this->db->get();
		$expense= $query->row()->transectionTotalAmount;
		return $expense;
	}
	
	public function total_return_by_dates($startDate,$endDate){
		$shop_id=$this->session->userdata('userShop');
		$this->db->select_sum('transectionTotalAmount');
		$this->db->from('transection_info');
		$this->db->where('transectionDate >=',$startDate);
		$this->db->where('transectionDate <=',$endDate);
		$this->db->where('shop_info_transectionShopID',$shop_id);
		$this->db->where('transectionType=3');
		$query = $this->db->get();
		$return= $query->row()->transectionTotalAmount;
		return $return;
	}
	
	
	public function get_return_report_by_dates($startDate,$endDate)
	{
		$shop_id=$this->session->userdata('userShop');
		$q=$this->db
				->select('exchange_info.*,shop_info.shopTitle')
				->from('exchange_info')
				->join('shop_info','exchange_info.shop_info_returnShopID=shop_info.shopID')
				->where('returnDate >=',$startDate)
				->where('returnDate <=',$endDate)
				->where('shop_info_returnShopID',$shop_id)
				//->limit($limit,$offset)
				->order_by("returnDate", "desc")
				->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	public function get_return_report($limit,$offset)
	{
		$shop_id=$this->session->userdata('userShop');
		$q=$this->db
				->select('exchange_info.*,product_info.productName')
				->from('exchange_info')
				->join('product_info','exchange_info.returnProductID=product_info.productID')
				->where('shop_info_returnShopID',$shop_id)
				->limit($limit,$offset)
				->order_by("returnDate", "desc")
				->get();
				
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_return_info_by_id($return_id)
	{
		$q=$this->db
				->select('exchange_info.*,shop_info.shopTitle')
				->from('exchange_info')
				->join('shop_info','exchange_info.shop_info_returnShopID=shop_info.shopID')
				->where('exchangeID',$return_id)
				->get();
				
		if($q->num_rows()==1){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	
	
	public function get_return_details_by_id($return_id)
	{
		$q=$this->db
				->select('exchange_details.*,product_info.productName,product_info.productBarcode,')
				->from('exchange_details')
				->join('product_info','exchange_details.exchangeProductID=product_info.productID')
				->where('exchange_info_exchangeID',$return_id)
				->where('exchangeProductType',0)
				->get();
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_return_exchange_details_by_id($return_id)
	{
		$q=$this->db
				->select('exchange_details.*,product_info.productName,product_info.productBarcode,')
				->from('exchange_details')
				->join('product_info','exchange_details.exchangeProductID=product_info.productID')
				->where('exchange_info_exchangeID',$return_id)
				->where('exchangeProductType',1)
				->get();
		if($q->num_rows()){
			return $q->result();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_expense_details_by_id($expense_id)
	{
		$q=$this->db
				->select('expense_info.*,transection_info.transectionID,admin_info.adminName,expense_field_info.expenseFieldName')
				->from('expense_info')
				->join('transection_info','expense_info.expenseID=transection_info.transectionReferenceID')
				->join('admin_info','expense_info.expenceEntryBy=admin_info.adminID')
				->join('expense_field_info','expense_info.expenceFieldID=expense_field_info.expenseFieldID')
				->where('expenseID',$expense_id)
				->where('transection_info.transectionType',2)
				->get();
				
		if($q->num_rows()==1){
			return $q->row();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_total_product_quantity_by_dates($startDate,$endDate){
		$shop_id=$this->session->userdata('userShop');
		$this->db->select_sum('saleProductQuantity');
		$this->db->from('sale_details');
		$this->db->join('sale_info','sale_info.saleID=sale_details.sale_info_saleID');
		$this->db->where('sale_info.saleDate >=',$startDate);
		$this->db->where('sale_info.saleDate <=',$endDate);
		$this->db->where('shop_info_shopID',$shop_id);
		$query = $this->db->get();
		$income= $query->row()->saleProductQuantity;
		return $income;
	}
	
    public function get_all_payable_accounts(){
		$data = $this->db
					->select('*')
					->from('client_info')
					->where('clientBalance>',0)
					->get();

		if($data){
			return $data->result();
		}
	}
	
	public function get_all_recieveable_accounts(){
		$data = $this->db
					->select('*')
					->from('client_info')
					->where('clientBalance<',0)
					->get();

		if($data){
			return $data->result();
		}
	}
	public function get_all_product_name_id(){
		$data = $this->db
					->select('productId,productName')
					->from('product_info')
					->get();

		if($data){
			return $data->result();
		}
	}
	
	public function get_all_product_info(){
		$data = $this->db
					->select('product_info.productId,productName,product_info.productQuantity,product_type_info.productTypeName')
					->from('product_info')
					->join('product_type_info','product_type_info.productTypeId=product_info.product_type_info_productTypeID')
					->limit(15)
					->get();

		if($data){
			return $data->result();
		}
	}
	
	public function get_all_product_type_name_id(){
		$data = $this->db
					->select('productTypeId,productTypeName')
					->from('product_type_info')
					->get();

		if($data){
			return $data->result();
		}
	}
	public function get_all_supplier_name_id(){
		$data = $this->db
					->select('clientID,clientContactName')
					->from('client_info')
					->where('clientType',2)
					->where('clientStatus',1)
					->get();

		if($data){
			return $data->result();
		}
	}
	/* Start Cash Account */
	public function get_all_cash_account_info(){
		$data = $this->db
					->select('*')
					->from('cash_account_info')
					->where('cashAccountInfoId>',0)
					->get();

		if($data){
			return $data->result();
		}
	}
	public function update_Cash_Account($data){
		return $this->db
			        ->where('cashAccountInfoId',$data['cashAccountInfoId'])
				    ->update('cash_account_info',$data);
	}
	public function update_client_address($data){
		return $this->db
			        ->where('clientAddressInfoId',$data['clientAddressInfoId'])
				    ->update('client_address_info',$data);
	}
	
	public function add_cash_account($data)
	{
		//print_r($data); exit();
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
        $date= $dt->format('Y-m-d H:i:s');
         $data['cashAccountAddedDate'] = $date;
         
         //$tim=time();
         //$data['cashAccountAddedDate']+=$tim;
         //print_r($data); exit();
		return $this->db->insert('cash_account_info', $data );
	}
	public function get_cash_account_info_by_id($id)
	{
		$data = $this->db
					->select('*')
					->from('cash_account_info')
					->where('cashAccountInfoId',$id)
					->get();

		if($data){
			return $data->row();
		}
	}
	/* End Cash Account */
	/* Start Received Payment*/
	public function store_received_payment($data)
	{
		$this->db->trans_start();
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
        $date= $dt->format('Y-m-d H:i:s');
        $data['cashPaymentDate'] = $date;
        $data['cashPaymentEntryBy'] = $this->session->userdata('admin_id');
		if($this->db->insert('cash_payment_info', $data )){
			$payment_id=$this->db->insert_id();
			if($data['paymentType']==1){
				$transection['transectionType']=11;
				$this->db
					->where('clientID',$data['client_info_paymentClientId'])
					->set('clientBalance', 'clientBalance + ' . $data['paidAmount'], FALSE)
					->update('client_info');
			}
			else{
				$transection['transectionType']=12;
				$this->db
					->where('clientID',$data['client_info_paymentClientId'])
					->set('clientBalance', 'clientBalance - ' . $data['paidAmount'], FALSE)
					->update('client_info');
			}
				
				$transection['transectionReferenceID']=$payment_id;
				$transection['transectionDetails']=$data['cashPaymentNote'];
				$transection['transectionBy']=$data['cashPaymentEntryBy'];
				$transection['transectionDate']=$data['cashPaymentDate'];
				$transection['transectionTotalAmount']=$data['paidAmount'];
				$this->db->insert('transection_info', $transection );
		}

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return false;
		else
			return true;
	}
	public function get_all_received_paid_paymant_info(){
		$data = $this->db
					->select('cashPaymentInfoId,paidAmount,paymentType,cashPaymentDate,client_info.clientContactName')
					->from('cash_payment_info')
					->join('client_info','cash_payment_info.client_info_paymentClientId=client_info.clientID')
					->order_by('cashPaymentDate','desc')
					->limit(15)
					->get();

		if($data){
			return $data->result();
		}
	}
	public function get_all_received_paid_paymant_info_by_id($id){
		$data = $this->db
					->select('cash_payment_info.*,client_info.clientID,client_info.clientContactName,client_info.clientContactNumber,client_info.clientBalance,client_address_info.clientAddress')
					->from('cash_payment_info')
					->join('client_info','cash_payment_info.client_info_paymentClientId=client_info.clientID')
					->join('client_address_info','client_info.clientID=client_address_info.client_info_clientID')
					->where('cashPaymentInfoId',$id)
					->where('client_address_info.clientAddressType',1)
					->get();

		if($data){
			return $data->row();
		}
		else return false;
	}
	/* End Received Payment */
	/*Start Bills */
	public function add_bill_info($data)
	{
		///$data['billDate']=date('Y-m-d',strtotime($data['billDate']));
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
        $date= $dt->format('Y-m-d H:i:s');
        $data['billAddedDate'] = $date;
        $data['billAddedBy'] = $this->session->userdata('admin_id');
         
         //$tim=time();
         //$data['cashAccountAddedDate']+=$tim;
         //print_r($data); exit();
		if($this->db->insert('bill_info', $data )){
		    return $this->db
						->where('clientID',$data['client_info_supplierId'])
						->set('clientBalance', 'clientBalance + ' . $data['billAmount'], FALSE)
						->update('client_info');
		}
		else return false;
	}
	
	public function add_debit_note($data)
	{
		$data['debitNoteDate']=date('Y-m-d',strtotime($data['debitNoteDate']));
		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
        $date= $dt->format('Y-m-d H:i:s');
        $data['debitNoteAddedDate'] = $date;
        $data['debitNoteAddedBy'] = $this->session->userdata('admin_id');
        //print_r($data); exit;
		if($this->db->insert('debit_note_info', $data )){
			$data['debitNoteAmount']=$data['debitNoteAmount']*(-1);
		    return $this->db
						->where('clientID',$data['client_info_debitNoteClientId'])
						->set('clientBalance', 'clientBalance + ' . $data['debitNoteAmount'], FALSE)
						->update('client_info');
		}
		else return false;
	}
	public function get_all_bill_info()
	{
		$data = $this->db
					->select('*')
					->from('bill_info')
					->get();

		if($data){
			return $data->result();
		}
	}
	public function get_bill_info_by_id($id)
	{
		$data = $this->db
					->select('*')
					->from('bill_info')
					->where('billInfoId',$id)
					->get();

		if($data){
			return $data->row();
		}
	}
	public function update_bill_info($data){
		return $this->db
			        ->where('billInfoId',$data['billInfoId'])
				    ->update('bill_info',$data);
	}
	/*End Bills*/
	public function get_all_client_type_name()
	{
		$data = $this->db
					->select('clientType, clientContactName')
					->from('client_info')
					->get();

		if($data){
			return $data->result();
		}
	}
	public function get_all_transaction_type_name_id()
	{
		$data = $this->db
					->select('transactionTypeId, transectionTypeName')
					->from('transection_type_info')
					->get();

		if($data){
			return $data->result();
		}
	}
	public function get_all_product_update()
	{
		$data = $this->db
					->select('product_update_info.productUpdateDate,product_update_info.productUpdatedQuantity,product_info.productName,product_info.productUnitName,admin_info.adminName,product_type_info.productTypeName')
					->from('product_update_info')
					->join('product_info','product_info.productId=product_update_info.product_info_updatedProductID')
					->join('product_type_info','product_info.product_type_info_productTypeID=product_type_info.productTypeID')
					->join('admin_info','admin_info.adminID=product_update_info.admin_info_productUpdatedBy')
					->get();

		if($data){
			return $data->result();
		}
	}
	public function get_all_sale_info()
	{
		$data = $this->db
					->select('sale_info.saleID,sale_info.saleDate,sale_info.saleTotalAmount,transection_type_info.*,')
					->from('sale_info')
					->join('transection_type_info','sale_info.saleType=transection_type_info.transactionTypeId')
					->get();

		if($data){
			return $data->result();
		}
	}
	public function get_all_sale_by_dates($data)
	{
		//$data['fromDate']= date('Y-m-d 00:00:00',strtotime($data['fromDate'])) ;
		//$data['toDate']= date('Y-m-d 00:00:00',strtotime($data['toDate'])) ;
		 $this->db
					->select('sale_info.saleID,sale_info.saleDate,sale_info.saleTotalAmount,transection_type_info.*,')
					->from('sale_info')
					->join('transection_type_info','sale_info.saleType=transection_type_info.transactionTypeId');
					if($data['fromDate']){
						$data['fromDate']= date('Y-m-d 00:00:00',strtotime($data['fromDate'])) ;
						$this->db->where('saleDate>=',$data['fromDate']);
					}
					if($data['toDate'])	{
						$data['toDate']= date('Y-m-d 23:59:59',strtotime($data['toDate'])) ; 
						$this->db->where('saleDate<=',$data['toDate']);
					}
						
					if($data['saleType'])
						$this->db->where('sale_info.saleType',$data['saleType']);

		$data =$this->db->get();

		if($data){
			return $data->result();
		}
	}
	public function get_all_cash_transfers_by_dates($data)
	{
		//$data['fromDate']= date('Y-m-d 00:00:00',strtotime($data['fromDate'])) ;
		//$data['toDate']= date('Y-m-d 00:00:00',strtotime($data['toDate'])) ;
		 $this->db
					->select('cash_transfer_details.*,from.cashAccountName as fromAccountName,to.cashAccountName as toAccountName')
					->from('cash_transfer_details')
					->join('cash_account_info as from','from.cashAccountInfoId=cash_transfer_details.cashTransferFrom',"LEFT")
					->join('cash_account_info as to','to.cashAccountInfoId=cash_transfer_details.cashTransferTo',"LEFT")
					->order_by('cashtransferDate','desc');
					if($data['fromDate']){
						$data['fromDate']= date('Y-m-d 00:00:00',strtotime($data['fromDate'])) ;
						$this->db->where('cashtransferDate>=',$data['fromDate']);
					}
					if($data['toDate'])	{
						$data['toDate']= date('Y-m-d 23:59:59',strtotime($data['toDate'])) ; 
						$this->db->where('cashtransferDate<=',$data['toDate']);
					}

		$data =$this->db->get();
		if($data){
			return $data->result();
		}
	}
	public function get_all_expenses_by_date($data)
	{
		//$data['fromDate']= date('Y-m-d 00:00:00',strtotime($data['fromDate'])) ;
		//$data['toDate']= date('Y-m-d 00:00:00',strtotime($data['toDate'])) ;
		 $this->db
					->select('*')
					->from('expense_info');
					
					if($data['fromDate']){
						$data['fromDate']= date('Y-m-d 00:00:00',strtotime($data['fromDate'])) ;
						$this->db->where('expenceDate>=',$data['fromDate']);
					}
					if($data['toDate'])	{
						$data['toDate']= date('Y-m-d 23:59:59',strtotime($data['toDate'])) ; 
						$this->db->where('expenceDate<=',$data['toDate']);
					}
						
				//	if($data['productId'])
						//$this->db->where('sale_info.product_info_updatedProductID',$data['productId']);

		$data =$this->db->get();

		if($data){
			return $data->result();
		}
	}
	public function get_all_product_update_by_dates($infos)
	{  
		$this->db
					->select('product_update_info.productUpdateDate,product_update_info.productUpdatedQuantity,product_info.productName,product_info.productUnitName,admin_info.adminName,product_type_info.productTypeName')
					->from('product_update_info')
					->join('product_info','product_info.productId=product_update_info.product_info_updatedProductID')
					->join('product_type_info','product_info.product_type_info_productTypeID=product_type_info.productTypeID')
					->join('admin_info','admin_info.adminID=product_update_info.admin_info_productUpdatedBy');
					if($infos['fromDate']){
						$infos['fromDate']= date('Y-m-d 00:00:00',strtotime($infos['fromDate'])) ;
						$this->db->where('productUpdateDate>=',$infos['fromDate']);
					}
					if($infos['toDate'])	{
						$infos['toDate']= date('Y-m-d 23:59:59',strtotime($infos['toDate'])) ; 
						$this->db->where('productUpdateDate<=',$infos['toDate']);
					}
						
					if($infos['productId'])
						$this->db->where('product_update_info.product_info_updatedProductID',$infos['productId']);

		$data =$this->db->get();

		if($data){
			return $data->result();
		}
	}
	public function get_income_expense_report($infos)
	{  
		$this->db
			 ->select('*')
			 ->from('transection_info')
			 ->join('transection_type_info','transection_info.transectionType=transection_type_info.transactionTypeId')
			 ->where('transectionDate>=',$infos['fromDate'])
			 ->where('transectionDate<=',$infos['toDate']);
		if($infos['transectionType']>0)
			$this->db->where('transection_info.transectionType',$infos['transectionType']);
		else
			$this->db->where("(transectionType = '1' OR transectionType = '2' OR transectionType = '3' OR transectionType = '8') ");
		$this->db->order_by('transectionDate','desc');
	    $data =$this->db->get();

		if($data){
			return $data->result();
		}
	}
	public function get_all_transaction_info($infos)
	{  
		$this->db
			 ->select('*')
			 ->from('transection_info')
			 ->join('transection_type_info','transection_info.transectionType=transection_type_info.transactionTypeId')
			 ->where('transectionDate>=',$infos['fromDate'])
			 ->where('transectionDate<=',$infos['toDate']);
		if($infos['transectionType']>0)
			$this->db->where('transection_info.transectionType',$infos['transectionType']);
			$this->db->order_by('transectionDate','desc');
	    $data =$this->db->get();

		if($data){
			return $data->result();
		}
	}



}