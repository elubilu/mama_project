<?php 
$config = array(
       'login_form' => array(
            array(
			   'field' => 'email',
               'label' => 'User Email',
               'rules' => 'required' 
            ),
            array(
               'field' => 'password',
               'label' => 'Password',
               'rules' => 'required|md5', 
            )
               
		),
		'add_user' => array(
            array(
			   'field' => 'adminName',
               'label' => 'Full Name',
               'rules' => 'required' 
            ),
            array(
               'field' => 'adminPassword',
               'label' => 'Password',
               'rules' => 'required|min_length[8]|md5', 
            ),
			array(
               'field' => 'confirmPass',
               'label' => 'Confirm Password',
               'rules' => 'required|min_length[8]|md5|matches[adminPassword]', 
            ),
            array(
               'field' => 'adminContact',
               'label' => 'Contact',
               'rules' => 'required', 
            ),
			array(
               'field' => 'adminUserID',
               'label' => 'User ID',
               'rules' => 'required|is_unique[admin_info.adminUserID]', 
            ),
			array(
               'field' => 'admin_role_roleID',
               'label' => 'User Type',
               'rules' => 'required', 
            ),
		),
		'update_user' => array(
            array(
			   'field' => 'adminName',
               'label' => 'Full Name',
               'rules' => 'required' 
            ),
			
            array(
               'field' => 'adminContact',
               'label' => 'Contact',
               'rules' => 'required', 
            ),
			
		),
		'add_sales_man' => array(
            array(
			   'field' => 'salesmanName',
               'label' => 'Full Name',
               'rules' => 'required' 
            ),
			array(
			   'field' => 'salesmanEmail',
               'label' => 'Email Address',
               'rules' => 'required|valid_email' 
            ),
            
            array(
               'field' => 'salesmanContact',
               'label' => 'Contact',
               'rules' => 'required|numeric', 
            ),
			array(
               'field' => 'salesmanAddress',
               'label' => 'Address',
               'rules' => 'required', 
            ),
			array(
               'field' => 'shop_info_shopID',
               'label' => 'Shop Name',
               'rules' => 'required', 
            ),
		),
		
		'update_sales_man' => array(
            array(
			   'field' => 'salesmanName',
               'label' => 'Full Name',
               'rules' => 'required' 
            ),
			array(
			   'field' => 'salesmanEmail',
               'label' => 'Email Address',
               'rules' => 'required|valid_email' 
            ),
            
            array(
               'field' => 'salesmanContact',
               'label' => 'Contact',
               'rules' => 'required|numeric', 
            ),
			array(
               'field' => 'salesmanAddress',
               'label' => 'Address',
               'rules' => 'required', 
            ),
			array(
               'field' => 'shop_info_shopID',
               'label' => 'Shop Name',
               'rules' => 'required', 
            ),
		),
		
		'add_supplier' => array(
            array(
			   'field' => 'supplierCompanyName',
               'label' => 'Company Name',
               'rules' => 'required' 
            ),
			array(
			   'field' => 'supplierContactName',
               'label' => 'Contact Name',
               'rules' => 'required' 
            ),
            
            array(
               'field' => 'supplierContact',
               'label' => 'Contact Number',
               'rules' => 'required', 
            ),
			array(
               'field' => 'supplierAddress',
               'label' => 'Address',
               'rules' => 'required', 
            ),
		),
		'add_new_product' => array(
			array(
			   'field' => 'productName',
               'label' => 'Product Name',
               'rules' => 'required' 
            ),
            array(
			   'field' => 'group_info_productGroupID',
               'label' => 'Group Name',
               'rules' => 'required' 
            ),
			array(
			   'field' => 'supplier_info_productSupplierID',
               'label' => 'Supplier Name',
               'rules' => 'required' 
            ),
            array(
               'field' => 'productPurchasePrice',
               'label' => 'Product Purchase Price',
               'rules' => 'required', 
            ),
			array(
               'field' => 'productSalePrice',
               'label' => 'Product Sale Price',
               'rules' => 'required', 
            ),
		),
		'add_product_to_stock' => array(
			array(
			   'field' => 'productID',
               'label' => 'Product',
               'rules' => 'required' 
            ),
			array(
			   'field' => 'productQuantity',
               'label' => 'Quantity',
               'rules' => 'required' 
            ),
            array(
               'field' => 'productPurchasePrice',
               'label' => 'Product Purchase Price',
               'rules' => 'required', 
            ),
			array(
               'field' => 'productSalePrice',
               'label' => 'Product Sale Price',
               'rules' => 'required', 
            ),
		),
		'update_product' => array(
			array(
			   'field' => 'productName',
               'label' => 'Product Name',
               'rules' => 'required' 
            ),
            array(
			   'field' => 'group_info_productGroupID',
               'label' => 'Group Name',
               'rules' => 'required' 
            ),
			array(
			   'field' => 'supplier_info_productSupplierID',
               'label' => 'Supplier Name',
               'rules' => 'required' 
            ),
            array(
               'field' => 'productPurchasePrice',
               'label' => 'Product Purchase Price',
               'rules' => 'required', 
            ),
			array(
               'field' => 'productSalePrice',
               'label' => 'Product Sale Price',
               'rules' => 'required', 
            ),
			array(
			   'field' => 'productQuantity',
               'label' => 'Quantity',
               'rules' => '' 
            ),
			array(
			   'field' => 'productStatus',
               'label' => 'Status',
               'rules' => 'required' 
            ),
		),
		
		'add_storeKeeper' => array(
            array(
			   'field' => 'adminName',
               'label' => 'Full Name',
               'rules' => 'required' 
            ),
			array(
			   'field' => 'adminEmail',
               'label' => 'Email Address',
               'rules' => 'required|valid_email' 
            ),
            array(
               'field' => 'adminPassword',
               'label' => 'Password',
               'rules' => 'required|min_length[8]|md5', 
            ),
			array(
               'field' => 'confirm_password',
               'label' => 'Confirm Password',
               'rules' => 'required|min_length[8]|md5|matches[adminPassword]', 
            ),
            array(
               'field' => 'adminContact',
               'label' => 'Contact',
               'rules' => 'required', 
            ),
			array(
               'field' => 'adminAddress',
               'label' => 'Address',
               'rules' => 'required', 
            ),
			array(
               'field' => 'adminUserID',
               'label' => 'User ID',
               'rules' => 'required', 
            ),
			array(
               'field' => 'admin_role_roleID',
               'label' => 'User Type',
               'rules' => 'required', 
            ),
		),
		
		'update_storeKeeper' => array(
            array(
			   'field' => 'adminName',
               'label' => 'Full Name',
               'rules' => 'required' 
            ),
			array(
			   'field' => 'adminEmail',
               'label' => 'Email Address',
               'rules' => 'required|valid_email' 
            ),
            array(
               'field' => 'adminContact',
               'label' => 'Contact',
               'rules' => 'required', 
            ),
			array(
               'field' => 'adminAddress',
               'label' => 'Address',
               'rules' => 'required', 
            ),
			array(
               'field' => 'adminStatus',
               'label' => 'Status',
               'rules' => '', 
            ),
		),
		
		'update_manager' => array(
            array(
			   'field' => 'adminName',
               'label' => 'Full Name',
               'rules' => 'required' 
            ),
			array(
			   'field' => 'adminEmail',
               'label' => 'Email Address',
               'rules' => 'required|valid_email' 
            ),
            array(
               'field' => 'adminContact',
               'label' => 'Contact',
               'rules' => 'required', 
            ),
			array(
               'field' => 'adminAddress',
               'label' => 'Address',
               'rules' => 'required', 
            ),
			array(
               'field' => 'adminStatus',
               'label' => 'Status',
               'rules' => '', 
            ),
			array(
               'field' => 'shop_info_shopID',
               'label' => 'Shop Name',
               'rules' => '', 
            ),
		),
		
		/*'edit_user' => array(
            array(
			   'field' => 'name',
               'label' => 'Full Name',
               'rules' => 'required' 
            ),
			array(
			   'field' => 'email',
               'label' => 'Email Address',
               'rules' => 'required|valid_email' 
            ),
            array(
               'field' => 'password',
               'label' => 'Password',
               'rules' => 'required|min_length[8]|md5', 
            ),
			array(
               'field' => 'confirm_password',
               'label' => 'Confirm Password',
               'rules' => 'required|min_length[8]|md5|matches[password]', 
            ),
            array(
               'field' => 'contact',
               'label' => 'Contact',
               'rules' => 'required', 
            ),
			array(
               'field' => 'admin_role_role_id',
               'label' => 'Role',
               'rules' => 'required', 
            ),
		),*/
		
		'update_password' => array(
            array(
               'field' => 'old_pass',
               'label' => 'Password',
               'rules' => 'required|min_length[8]|md5', 
            ),
			array(
               'field' => 'new_pass',
               'label' => 'Password',
               'rules' => 'required|min_length[8]|md5', 
            ),
			array(
               'field' => 'confirm_pass',
               'label' => 'Confirm Password',
               'rules' => 'required|min_length[8]|md5|matches[new_pass]', 
            ),
		),
	'update_customer_password' => array(
            
			array(
               'field' => 'password',
               'label' => 'Password',
               'rules' => 'required|min_length[8]|md5', 
            ),
			array(
               'field' => 're_pass',
               'label' => 'Confirm Password',
               'rules' => 'required|min_length[8]|md5|matches[password]', 
            ),
		),
	'contact_us' => array(
            
         array(
               'field' => 'password',
               'label' => 'Password',
               'rules' => 'required|min_length[8]|md5', 
            ),
         array(
               'field' => 're_pass',
               'label' => 'Confirm Password',
               'rules' => 'required|min_length[8]|md5|matches[password]', 
            ),
      ),
   'addClients' => array(
            
         array(
               'field' => 'clientContactNumber',
               'label' => 'Contact Number',
               'rules' => 'required|is_unique[client_info.clientContactNumber]', 
            ),
         array(
               'field' => 'clientBalance',
               'label' => 'Previous Balance',
               'rules' => 'required', 
            ), 
         array(
               'field' => 'clientNote',
               'label' => 'PSpecial Note About Client',
               'rules' => 'required', 
            ), 
         array(
               'field' => 'address',
               'label' => 'Address',
               'rules' => 'required', 
            ),
         array(
               'field' => 'addressTitle',
               'label' => 'Address Title',
               'rules' => 'required', 
            ),
            array(
               'field' => 'clientContactName',
               'label' => 'Contact Name',
               'rules' => 'required', 
            ),
         
      ),
   'addProduct' => array(
            
         array(
               'field' => 'productName',
               'label' => 'Product Name',
               'rules' => 'required|is_unique[product_info.productName]', 
            ),    
         array(
               'field' => 'productQuantityID',
               'label' => 'Quantity',
               'rules' => 'required', 
            ),    
         array(
               'field' => 'productTotalCostID',
               'label' => 'Total Cost',
               'rules' => 'required', 
            ),    
         array(
               'field' => 'productUnitCostID',
               'label' => 'Unit Cost',
               'rules' => 'required', 
            ),
         
      ),
   'updateAdmin' => array(
            
         array(
               'field' => 'adminUserID',
               'label' => 'User Name',
               'rules' => 'required|is_unique[product_info.productName]', 
            ),    
         array(
               'field' => 'adminPassword',
               'label' => 'Password',
               'rules' => 'required|md5', 
            ),    
          array(
               'field' => 'adminPasswordOld',
               'label' => 'Password',
               'rules' => 'md5', 
            ),    
          array(
               'field' => 'adminPasswordNew',
               'label' => 'Password',
               'rules' => 'md5', 
            ),    
          array(
               'field' => 'adminPasswordConfirm',
               'label' => 'Password',
               'rules' => 'md5|matches[adminPasswordConfirm]', 
            ),    
          
      ),

   'storeExpense' => array(
            
         array(
               'field' => 'expenceReference',
               'label' => 'Reference',
               'rules' => 'required|is_unique[expense_info.expenceReference]', 
            ),       
          array(
               'field' => 'expenceAmount',
               'label' => 'Amount',
               'rules' => 'required', 
            ),    
          array(
               'field' => 'expenceFieldID',
               'label' => 'Expense Name',
               'rules' => 'required', 
            ),    
          array(
               'field' => 'expenseNote',
               'label' => 'Note',
               'rules' => 'required', 
            ),    
          
      ),
   'storeFieldExpense' => array(
                   
             
          array(
               'field' => 'expenseFieldName',
               'label' => 'Expense Field Name',
               'rules' => 'required', 
            ),    
          array(
               'field' => 'expenseFieldDetails',
               'label' => 'Details',
               'rules' => 'required', 
            ),    
          
      ),
   'storeCashTransfer' => array(
            
         array(
               'field' => 'cashTransferTo',
               'label' => 'To Account',
               'rules' => 'required', 
            ),       
          array(
               'field' => 'cashTransferAmount',
               'label' => 'Amount',
               'rules' => 'required', 
            ),    
          array(
               'field' => 'cashTransferFrom',
               'label' => 'From Account',
               'rules' => 'required', 
            ),    
          array(
               'field' => 'cashTransferNote',
               'label' => 'Note',
               'rules' => 'required', 
            ),    
          
      ),

	);
	
	
	
?>	
	