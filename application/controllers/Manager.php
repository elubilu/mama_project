<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manager extends MY_Controller {

	public function __construct(){
		parent:: __construct();
		$this->load->model('manager_model');
		if($this->session->userdata('user_role')!=4)return redirect('login/');
	}

	public function index(){
		$totalCash=$this->manager_model->get_total_cash_amount();
		$this->load->view('manager/dashBoard',['totalCash'=>$totalCash]);
	}
	/* My Account */	
	public function myAccount(){
		$id=$this->session->userdata('admin_id');
		$data=$this->manager_model->get_user_details_by_id($id);
		//print_r($id); exit();
		$this->load->view('manager/myAccount',['data'=>$data]);
	}
	/* Start All Sale */
	public function addInvoice(){
		$this->cart->destroy();
		$data=$this->manager_model->get_all_client_name_id();
		$this->load->view('manager/addInvoice',['clients'=>$data]);
	}
	public function storeInvoice(){
		$data=$this->input->post();
		if($cart=$this->cart->contents()){
			$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
			$date= $dt->format('Y-m-d H:i:s');
			$data['saleDate']=$date;
			$cart=$this->cart->contents();
			$sale_id=$this->manager_model->store_invoice($data,$cart);
			if($sale_id){
				$this->cart->destroy();
				$this->session->set_flashdata('feedback_successfull', 'Sale Done Successfull');
				redirect($this->agent->referrer());
			}
			else{
				$this->session->set_flashdata('feedback_failed', 'Plase Try Again');
				$data=$this->manager_model->get_all_client_name_id();
				$this->load->view('manager/addInvoice',['clients'=>$data]);
			}
		}
		else{
			$this->session->set_flashdata('feedback_failed', 'Select a Product to Sale');
			$data=$this->manager_model->get_all_client_name_id();
			$this->load->view('manager/addInvoice',['clients'=>$data]);
		}
		//print_r($data);
		
	}
	public function ajax_getCustomerInfo(){
		
		$info=$this->manager_model->ajax_get_client_contact_by_id($this->input->post('client_id'));
		$data=$this->manager_model->ajax_get_client_last_five_transaction($this->input->post('client_id'));
		if($info){
			$data = array(
				'status'=>true, 
				'clientContactNumber'=>$info->clientContactNumber,
				'clientBalance'=>$info->clientBalance,
				'clientAddress'=>$info->clientAddress,
				'infos'=>$data
			);
		}
		else{
			$data = array(
				'status'=>false,
			);
		}
		echo json_encode($data);
	}
	public function ajax_addProductToCart(){
		$product=$this->input->post();
		$product_details=$this->manager_model->ajax_get_product_details_by_id($this->input->post('product_id'));
		if($this->input->post('product_qty')){
			$qty=$this->input->post('product_qty');
		}
		
		$pastBuy=0;
		$cartContent=$this->cart->contents();
		if($cartContent !=null){
			foreach($cartContent as $content){
				if($content['id'] == $product_details->productId){
					$pastBuy = $content['qty'];
				}
			}
		}
		if(($qty+$pastBuy)>$product_details->productQuantity){
			$data = array(
				'errors' => 'Available: '.($product_details->productQuantity-$pastBuy),
				'status'=>false
			);
		}
		else{
			
			$insert=array(
				'id'=>$product_details->productId,
				'qty'=>$qty,
				'salePrice'=>$this->input->post('product_special_rate'),
				'price'=>$product_details->productSalePrice,
				'name'=>$product_details->productName,
				'total_quantity'=>$product_details->productQuantity,
			);
			
			$this->cart->insert($insert);	
			$cart=$this->cart->contents();
			//$data['status'] = true; //Set response  
			$numItems = count($this->cart->contents());
			$i = 0;
			foreach($this->cart->contents() as $key) { 
			if($i+1 == $numItems) { 
			$saved_rowid = $key['rowid']; 
			} 
			$i++; 
			}
			
			if($pastBuy>0){
				$data = array(
				'product_id'=>$product_details->productId,
				'quantity'=>$qty+$pastBuy,
				'salePrice'=>$this->input->post('product_special_rate'),
				'price'=>$product_details->productSalePrice,
				'total_quantity'=>$product_details->productQuantity,
				'rowid'=>$saved_rowid,
				'status'=>true,
				'past_buy'=>true
				);
			}
			else{
				$data = array(
				'product_id'=>$product_details->productId,
				'quantity'=>$qty,
				'salePrice'=>$this->input->post('product_special_rate'),
				'name'=>$product_details->productName,
				'total_quantity'=>$product_details->productQuantity,
				'price'=>$product_details->productSalePrice,
				'rowid'=>$saved_rowid,
				'status'=>true,
				'past_buy'=>false
				);
			}
        }    
		echo json_encode($data);
	}
	public function ajaxCartRemove(){
	
		if($this->cart->update(array('rowid'=>$this->input->post('row_id'),'qty'=>0))){
			$data = array(
					'status'=>true, 
			);
			echo json_encode($data);
		}
		else{
			$data = array(
					'status'=>false, 
			);
			echo json_encode($data);
		}
			
	}
	public function selectCustomerModal(){
		$this->load->view('manager/selectCustomerModal');
	}
	public function addQuickSale(){
		$this->cart->destroy();
		$data=$this->manager_model->get_all_client_name_id();
		$this->load->view('manager/addQuickSale',['clients'=>$data]);
	}
	
	public function ajax_addQuickSaleProductToCart(){
		$product=$this->input->post();
		$product_details=$this->manager_model->ajax_get_product_details_by_id($this->input->post('product_id'));
		if($this->input->post('product_qty')){
			$qty=$this->input->post('product_qty');
		}
		
		$pastBuy=0;
		$cartContent=$this->cart->contents();
		if($cartContent !=null){
			foreach($cartContent as $content){
				if($content['id'] == $product_details->productId){
					$pastBuy = $content['qty'];
				}
			}
		}
		if(($qty+$pastBuy)>$product_details->productQuantity){
			$data = array(
				'errors' => 'Available: '.($product_details->productQuantity-$pastBuy),
				'status'=>false
			);
		}
		else{
			
			$insert=array(
				'id'=>$product_details->productId,
				'qty'=>$qty,
				'salePrice'=>$this->input->post('product_special_rate'),
				'price'=>$product_details->productSalePrice,
				'buy_price'=>$this->input->post('product_buy_price'),
				'name'=>$product_details->productName,
				'total_quantity'=>$product_details->productQuantity,
			);
			
			$this->cart->insert($insert);	
			$cart=$this->cart->contents();
			//$data['status'] = true; //Set response  
			$numItems = count($this->cart->contents());
			$i = 0;
			foreach($this->cart->contents() as $key) { 
				if($i+1 == $numItems) { 
				$saved_rowid = $key['rowid']; 
				} 
				$i++; 
			}
			
			if($pastBuy>0){
				$data = array(
				'product_id'=>$product_details->productId,
				'quantity'=>$qty+$pastBuy,
				'salePrice'=>$this->input->post('product_special_rate'),
				'price'=>$product_details->productSalePrice,
				'total_quantity'=>$product_details->productQuantity,
				'rowid'=>$saved_rowid,
				'status'=>true,
				'past_buy'=>true
				);
			}
			else{
				$data = array(
				'product_id'=>$product_details->productId,
				'quantity'=>$qty,
				'salePrice'=>$this->input->post('product_special_rate'),
				'name'=>$product_details->productName,
				'total_quantity'=>$product_details->productQuantity,
				'price'=>$product_details->productSalePrice,
				'rowid'=>$saved_rowid,
				'status'=>true,
				'past_buy'=>false
				);
			}
        }    
		echo json_encode($data);
	}
	
	public function storeQuickSale(){
		$data=$this->input->post();
		if($cart=$this->cart->contents()){
			$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
			$date= $dt->format('Y-m-d H:i:s');
			$data['saleDate']=$date;
			$cart=$this->cart->contents();
			$sale_id=$this->manager_model->store_quick_sale($data,$cart);
			if($sale_id){
				$this->cart->destroy();
				$this->session->set_flashdata('feedback_successfull', 'Quick Sale Done Successfull');
				redirect($this->agent->referrer());
			}
			else{
				$this->session->set_flashdata('feedback_failed', 'Plase Try Again');
				$data=$this->manager_model->get_all_client_name_id();
				$this->load->view('manager/addQuickSale',['clients'=>$data]);
			}
		}
		else{
			$this->session->set_flashdata('feedback_failed', 'Select a Product to Sale');
			$data=$this->manager_model->get_all_client_name_id();
			$this->load->view('manager/addQuickSale',['clients'=>$data]);
		}
		//print_r($data);
		
	}
	
	public function viewSaleDetails($sale_id){
		$sale_info=$this->manager_model->get_sale_info_by_id($sale_id);
		//print_r($sale_info);exit;
		$sale_details=$this->manager_model->get_sale_info_details_by_id($sale_id);
		$this->load->view('manager/viewSaleDetails',['info'=>$sale_info,'details'=>$sale_details]);
	}
	public function viewQuickSaleDetails($sale_id){
		$sale_info=$this->manager_model->get_sale_info_by_id($sale_id);
		//print_r($sale_info);exit;
		$sale_details=$this->manager_model->get_quick_sale_info_details_by_id($sale_id);
		$this->load->view('manager/viewQuickSaleDetails',['info'=>$sale_info,'details'=>$sale_details]);
	}
	public function viewExchangeDetails(){
		$this->load->view('manager/viewExchangeDetails');
	}
	
	
	/* End All Sale*/
	/*Start Retun*/
	public function addNewReturn(){
		$this->cart->destroy();
		$data=$this->manager_model->get_all_client_name_id();
		$product=$this->manager_model->get_all_active_product_id_name();
		$this->load->view('manager/addNewReturn',['clients'=>$data,'products'=>$product]);
	}
	public function ajax_addReturnProductToCart(){
		$product=$this->input->post();
		$product_details=$this->manager_model->ajax_get_product_details_by_id($this->input->post('product_id'));
		//print_r($product_details);exit;
		if($this->input->post('product_qty')){
			$qty=$this->input->post('product_qty');
		}
		
		$pastBuy=0;
		$cartContent=$this->cart->contents();
		if($cartContent !=null){
			foreach($cartContent as $content){
				if($content['id'] == $product_details->productId){
					$pastBuy = $content['qty'];
				}
			}
		}
		if(($qty+$pastBuy)>$product_details->productQuantity){
			$data = array(
				'errors' => 'Available: '.($product_details->productQuantity-$pastBuy),
				'status'=>false
			);
		}
		else{
			
			$insert=array(
				'id'=>$product_details->productId,
				'qty'=>$qty,
				//'salePrice'=>$this->input->post('product_special_rate'),
				'price'=>$product_details->productSalePrice,
				//'buy_price'=>$this->input->post('product_buy_price'),
				'name'=>$product_details->productName,
				'total_quantity'=>$product_details->productQuantity,
				'product_type'=>0,
			);
			
			$this->cart->insert($insert);	
			$cart=$this->cart->contents();
			//$data['status'] = true; //Set response  
			$numItems = count($this->cart->contents());
			$i = 0;
			foreach($this->cart->contents() as $key) { 
				if($i+1 == $numItems) { 
				$saved_rowid = $key['rowid']; 
				} 
				$i++; 
			}
			
			if($pastBuy>0){
				$data = array(
				'product_id'=>$product_details->productId,
				'quantity'=>$qty+$pastBuy,
				//'salePrice'=>$this->input->post('product_special_rate'),
				'price'=>$product_details->productSalePrice,
				'total_quantity'=>$product_details->productQuantity,
				'rowid'=>$saved_rowid,
				'status'=>true,
				'past_buy'=>true
				);
			}
			else{
				$data = array(
				'product_id'=>$product_details->productId,
				'quantity'=>$qty,
				//'salePrice'=>$this->input->post('product_special_rate'),
				'name'=>$product_details->productName,
				'total_quantity'=>$product_details->productQuantity,
				'price'=>$product_details->productSalePrice,
				'rowid'=>$saved_rowid,
				'status'=>true,
				'past_buy'=>false
				);
			}
        }    
		echo json_encode($data);
	}
	
	public function ajax_addExchangeProductToCart(){
		$product=$this->input->post();
		$product_details=$this->manager_model->ajax_get_product_details_by_id($this->input->post('product_id'));
		//print_r($product_details);exit;
		if($this->input->post('product_qty')){
			$qty=$this->input->post('product_qty');
		}
		
		$pastBuy=0;
		$cartContent=$this->cart->contents();
		if($cartContent !=null){
			foreach($cartContent as $content){
				if($content['id'] == $product_details->productId){
					$pastBuy = $content['qty'];
				}
			}
		}
		if(($qty+$pastBuy)>$product_details->productQuantity){
			$data = array(
				'errors' => 'Available: '.($product_details->productQuantity-$pastBuy),
				'status'=>false
			);
		}
		else{
			
			$insert=array(
				'id'=>$product_details->productId,
				'qty'=>$qty,
				'price'=>$this->input->post('product_special_rate'),
				'mainPrice'=>$product_details->productSalePrice,
				//'buy_price'=>$this->input->post('product_buy_price'),
				'name'=>$product_details->productName,
				'total_quantity'=>$product_details->productQuantity,
				'product_type'=>1,
			);
			
			$this->cart->insert($insert);	
			$cart=$this->cart->contents();
			//$data['status'] = true; //Set response  
			$numItems = count($this->cart->contents());
			$i = 0;
			foreach($this->cart->contents() as $key) { 
				if($i+1 == $numItems) { 
				$saved_rowid = $key['rowid']; 
				} 
				$i++; 
			}
			
			if($pastBuy>0){
				$data = array(
				'product_id'=>$product_details->productId,
				'quantity'=>$qty+$pastBuy,
				//'salePrice'=>$this->input->post('product_special_rate'),
				'price'=>$product_details->productSalePrice,
				'total_quantity'=>$product_details->productQuantity,
				'rowid'=>$saved_rowid,
				'status'=>true,
				'past_buy'=>true
				);
			}
			else{
				$data = array(
				'product_id'=>$product_details->productId,
				'quantity'=>$qty,
				//'salePrice'=>$this->input->post('product_special_rate'),
				'name'=>$product_details->productName,
				'total_quantity'=>$product_details->productQuantity,
				'price'=>$product_details->productSalePrice,
				'rowid'=>$saved_rowid,
				'status'=>true,
				'past_buy'=>false
				);
			}
        }    
		echo json_encode($data);
	}
	public function storeReturnProduct(){
		$data=$this->input->post();
		$cart=$this->cart->contents();
		if($cart){
			//print_r($data); exit;
			//$cart=$this->cart->contents();
			$sale_id=$this->manager_model->store_return_product($data,$cart);
			if($sale_id){
				$this->cart->destroy();
				$this->session->set_flashdata('feedback_successfull', 'Return Transaction Successfull');
				redirect($this->agent->referrer());
			}
			else{
				$this->session->set_flashdata('feedback_failed', 'Please Try Again');
				redirect($this->agent->referrer());
			}
		}
		else{
			$this->session->set_flashdata('feedback_failed', 'Please Select Products for Exchange');
			$this->load->view('manager/addNewReturn');
		}
	}
	/*End Return*/
	public function allPayments(){
		$data=$this->manager_model->get_all_received_paid_paymant_info();
		$this->load->view('manager/allPayments',['data'=>$data]);
	}
	public function receivedPaidPaymentForm(){
		$data=$this->manager_model->get_all_type_clients_id_name();
		$this->load->view('manager/receivedPaidPaymentForm',['data'=>$data]);
	}
	public function storeReceivedPaidPayment(){
		$data=$this->input->post();
		//print_r($data);exit;
		if($this->manager_model->store_received_payment($data)){
			$this->session->set_flashdata('feedback_successfull', 'Payment has Done Successfully');
			return redirect('manager/allPayments');
		}
		else{
			$this->session->set_flashdata('feedback_failed', 'Please Try Again!');
			return redirect('manager/allPayments');
		}
		//$data=$this->manager_model->get_all_type_clients_id_name();
		//$this->load->view('manager/receivedPaidPaymentForm',['data'=>$data]);
	}
	public function viewReceivedPaidPaymentForm($id){
		$data=$this->manager_model->get_all_received_paid_paymant_info_by_id($id);
		$this->load->view('manager/viewReceivedPaidPaymentForm',['data'=>$data]);
	}
	/* Start Expense */
	public function expenses(){
		$dates=$this->input->post();
		if($dates){
			$data=$this->manager_model->get_all_expenses_by_date($dates);
		}
		else
		{
			$data=$this->manager_model->get_all_expenses();
		}
		
		$this->load->view('manager/expenses',['data'=>$data]);
	}
	public function addExpense(){
		$data=$this->manager_model->get_all_expense_details();
		//print_r($data); exit();
		$this->load->view('manager/addExpense',['data1'=>$data]);
	}
	public function viewExpense($id){
		$data=$this->manager_model->get_expence_info_by_id($id);
		//print_r($data); exit();
		$this->load->view('manager/viewExpense',['data'=>$data]);
	}
	public function storeExpenseInfo(){

		$data=$this->input->post();
		$this->load->library('form_validation');
		if ($this->form_validation->run('storeExpense') == FALSE){
			//print_r($data); exit();
			$this->load->view('manager/addExpense');
        }
		else{
			
			if($this->manager_model->store_expense_info($data)){
				$this->session->set_flashdata('feedback_successfull', 'Added New Expense Successfully');
				return redirect('manager/expenses');
			}
			else{
				$this->session->set_flashdata('feedback_failed', 'Add New Expense Failed!');
				return redirect('manager/addExpense');
			}
		}
	}
	public function storeExpenseFields(){

		$data=$this->input->post();
		$this->load->library('form_validation');
		if ($this->form_validation->run('storeFieldExpense') == FALSE){
			//print_r($data); exit();
			$data=$this->manager_model->get_all_expense_details();
			$this->load->view('manager/expenseFileds',['data'=>$data]);
        }
		else{
			
			if($this->manager_model->store_expense_field_info($data)){
				$this->session->set_flashdata('feedback_successfull', 'Added New Expense Field Successfully');
				return redirect('manager/expenseFileds');
			}
			else{
				$this->session->set_flashdata('feedback_failed', 'Add New Expense Field Failed!');
				return redirect('manager/expenseFileds');
			}
		}
	}
	public function expenseFileds(){
		$data=$this->manager_model->get_all_expense_details();
		$this->load->view('manager/expenseFileds',['data'=>$data]);
	}
	public function updateExpenseFields(){
		$value=$this->input->post();
		if($this->manager_model->update_expense_field($value))
		{
			$data=$this->manager_model->get_all_expense_details();
			$this->load->view('manager/expenseFileds',['data'=>$data]);
		}
		else redirect('manager/editExpenseField') ;
	}
	public function editExpenseField($id){
		$data=$this->manager_model->get_expence_field_info_by_id($id);
		$this->load->view('manager/editExpenseField',['value'=>$data]);
	}
	
	/* End Expense */
	/* Start Cash Transfer */
	public function cashTransfers(){
		$data=$this->input->post();
		if($data)
		{
			$value=$this->manager_model->get_all_cash_transfers_by_dates($data);

		}
		else 
		$value=$this->manager_model->get_all_cash_transfers();
		//print_r($data); exit();
		$this->load->view('manager/cashTransfers',['data'=>$value]);
	}
	public function viewCashTransfer($id){
		$value=$this->manager_model->get_transfer_details_by_id($id);
		$this->load->view('manager/viewCashTransfer',['value'=>$value]);
	}
	public function newCashTransfer(){
		$data=$this->manager_model->get_all_cash_account_name_id();
		$this->load->view('manager/newCashTransfer',['data'=>$data]);
	}	
	public function storeNewCashTransfer(){
		$data=$this->input->post();
		$this->load->library('form_validation');
		if ($this->form_validation->run('storeCashTransfer') == FALSE){
			$accounts=$this->manager_model->get_all_cash_account_name_id();
			$this->load->view('manager/newCashTransfer',['data'=>$accounts]);
        }
		else{
			if($this->manager_model->store_cash_transfer($data)){
				$this->session->set_flashdata('feedback_successfull', 'Cash Transfer Successfull');
				return redirect('manager/cashTransfers');
			}
			else{
				$this->session->set_flashdata('feedback_failed', 'Cash Transfer Failed!');
				return redirect('manager/newCashTransfer');
			}
		}
		//$this->load->view('manager/addNewProductToInventory');
	}
	
	/* End Cash Transfer */
	
	/* Start Cash in out */	
	public function storeCashInOut(){
		$data=$this->input->post();
		$this->load->library('form_validation');
		/*if ($this->form_validation->run('storeCashTransfer') == FALSE){
			$accounts=$this->manager_model->get_all_cash_account_name_id();
			$this->load->view('manager/newCashTransfer',['data'=>$accounts]);
        }
		else{*/
			if($this->manager_model->store_cash_in_out($data)){
				$this->session->set_flashdata('feedback_successfull', 'Cash Transection Successfull');
				return redirect('manager/CashInOut');
			}
			else{
				$this->session->set_flashdata('feedback_failed', 'Cash Transection Failed!');
				return redirect('manager/CashInOut');
			}
		//}
		//$this->load->view('manager/addNewProductToInventory');
	}
	public function CashInOut(){
		$this->load->view('manager/CashInOut');
	}
	public function viewCashInOut(){
		$this->load->view('manager/viewCashInOut');
	}
	/* End Cash in out*/
	/* Start Cash Account*/
	public function allCashAccount(){
		$data=$this->manager_model->get_all_cash_account_info();
		$this->load->view('manager/allCashAccount',['data'=>$data]);
	}
	public function addCashAccount(){
		$this->load->view('manager/addCashAccount');
	}
	public function viewCashAccount($id){
		$data=$this->manager_model->get_cash_account_info_by_id($id);
		$infos=$this->manager_model->get_transaction_info_by_id($id);
		//print_r($infos); exit();
		$this->load->view('manager/viewCashAccount',['data'=>$data,'infos'=>$infos]);
	}
	/* End Cash Account*/
	/* Start Product info*/	
	
	public function showInventory(){
		$pName=$this->manager_model->get_all_product_name_id();
		$data=$this->manager_model->get_all_product_info();
		$pType=$this->manager_model->get_all_product_type_name_id();
		$sName=$this->manager_model->get_all_supplier_name_id();
		if($this->input->post()){
			$data=$this->input->post();
			print_r($data);exit;
		}
      //print_r($pName); exit();
		$this->load->view('manager/showInventory',['pName'=>$pName,'pType'=>$pType, 'sName'=>$sName,'products'=>$data]);
	}
	public function transferProduct(){
		$this->load->view('manager/transferProduct');
	}
	public function addNewProductToInventory(){
		$data_all_product_type = $this->manager_model->all_product_type(); 
		$data_all_suppliers = $this->manager_model->get_all_suppliers(); 
		$this->load->view('manager/addNewProductToInventory',['data_pro_type'=>$data_all_product_type, 'data_supplier'=>$data_all_suppliers]);
	}
	public function storeNewProductToInventory(){
		$data=$this->input->post();
		$this->load->library('form_validation');
		if ($this->form_validation->run('addProduct') == FALSE){
			$this->load->view('manager/addNewProductToInventory');
        }
		else{
			if($this->manager_model->store_product_to_inventory($data)){
				$this->session->set_flashdata('feedback_successfull', 'Added New Product Successfully');
				return redirect('manager/showInventory');
			}
			else{
				$this->session->set_flashdata('feedback_failed', 'Add New Product Failed!');
				return redirect('manager/addNewProductToInventory');
			}
		}
		//$this->load->view('manager/addNewProductToInventory');
	}
	
	public function addProductToStock(){
		$data=$this->manager_model->get_all_active_product_id_name();
		$this->load->view('manager/addProductToStock',['infos'=>$data]);
	}
	public function storeProducToStock(){
		$data=$this->input->post();
		$product_id=$this->input->post('productId');
		$product_quantity=$this->input->post('productQuantity');
		unset($data['productId'],$data['productQuantity']);
		//print_r($data);exit;
		if($this->manager_model->add_product_to_stock($data,$product_id,$product_quantity)){
			$this->session->set_flashdata('feedback_successfull', 'Added Product Successfully');
			return redirect('manager/showInventory');
		}
		else{
			$this->session->set_flashdata('feedback_failed', 'Add Product Failed!');
			return redirect('manager/addProductToStock');
		}
		//$data=$this->manager_model->get_all_active_product_id_name();
		//$this->load->view('manager/addProductToStock',['infos'=>$data]);
	}
	public function productDetails($id){
		$data=$this->manager_model->get_products_details_by_id($id);
		$this->load->view('manager/productDetails',['data'=>$data]);
	}
	public function ajax_productQuantityInfo(){
		
		$info=$this->manager_model->ajax_get_product_quantity_by_id($this->input->post('product_id'));
		if($info){
			$data = array(
				'status'=>true, 
				'productQuantity'=>$info->productQuantity,
				'productPurchasePrice'=>$info->productPurchasePrice,
				'productSalePrice'=>$info->productSalePrice
			);
		}
		else{
			$data = array(
				'status'=>false,
			);
		}
		echo json_encode($data);
	}
	public function ajax_getSearchProductInfo(){
		$data=$this->input->post();
		//print_r($data);exit;
		$info=$this->manager_model->ajax_get_inventory_product_serach_info($data);
		if($info){
			$data = array(
				'status'=>true, 
				'infos'=>$info
			);
		}
		else{
			$data = array(
				'status'=>false,
			);
		}
		echo json_encode($data);
	}
	
	
	public function ajaxProductInfoSearch(){
		
		$keyword = $this->input->post('term');  
		$strup = ucwords($keyword);
		$strlow =strtolower($keyword);
		$strupper =strtoupper($keyword);
		$strfirst =ucfirst($keyword);
        $data['response'] = 'false'; //Set default response  
        $query = $this->manager_model->ajax_get_product_info_search($keyword,$strup,$strlow,$strupper,$strfirst); //Search DB  
        if( ! empty($query) )  
        {  
            $data['response'] = 'true'; //Set response  
            $data['message'] = array(); //Create array  
            $data['m'] = array(); //Create array  
            foreach( $query as $row )  
            {  
                $data['message'][] = array(   
                                          
                                        'value' => $row->productId,  
                                        'label' => $row->productName
                                     );  //Add a row to array  
									 $data['m']=$row->productName;
            }  
        }  
        //print_r($data); exit;
        if('IS_AJAX')  
        {  
            //echo json_encode($data); //echo json string if ajax request  
            echo json_encode($data, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
        }  
        else 
        {  
            $this->load->view('manager/dashBoard',$data); //Load html view of search results  
        }
	}
	/* Product Type */
	public function addNewProductTypeToInventory(){
		$this->load->view('manager/addNewProductTypeToInventory');
	}
	public function storeProductType(){
		$data=$this->input->post();
		if($this->manager_model->store_product_type($data)){
			$this->session->set_flashdata('feedback_successfull', 'Added New Product Type Successfully');
			return redirect('manager/productType');
		}
		else{
			$this->session->set_flashdata('feedback_failed', 'Add Product Type Failed!');
			return redirect('manager/addNewProductTypeToInventory');
		}
		//$this->load->view('manager/addNewProductTypeToInventory');
	}
	public function productType(){
		$data=$this->manager_model->get_all_productType();
		$this->load->view('manager/productType',['data'=>$data]);
	}

	public function viewProductType($id){
		$data=$this->manager_model->get_productType_details_by_ID($id);
		$this->load->view('manager/viewProductType',['data'=>$data]);
	}

	public function updateProductType(){
		$data=$this->input->post();
		//print_r($data);exit;
		if($this->manager_model->update_view_product_type($data)){
			redirect('manager/productType');
		}
		else{
			redirect('manager/productType');
		}
	}
	
	/* End Product info*/
	
	public function accountPayables(){
		$data=$this->manager_model->get_all_payable_accounts();
		//print_r($data); exit();
		$this->load->view('manager/accountPayables',['data'=>$data]);
	}
	public function supplierProfile($id){
		$data=$this->manager_model->get_client_details_by_ID($id);
		$address=$this->manager_model->get_client_address_details_by_ID($id);
		$transactions=$this->manager_model->get_client_transaction_details_by_ID($id);
		$this->load->view('manager/supplierProfile',['data'=>$data,'addresses'=>$address,'transactions'=>$transactions]);
	}
	public function accountReceivables(){
		$data=$this->manager_model->get_all_recieveable_accounts();
		//print_r($data); exit();
		$this->load->view('manager/accountReceivables',['data'=>$data]);
	}
	public function clientProfile($id){

		$data=$this->manager_model->get_client_details_by_ID($id);
		$address=$this->manager_model->get_client_address_details_by_ID($id);
		$transactions=$this->manager_model->get_client_transaction_details_by_ID($id);
		$this->load->view('manager/clientProfile',['data'=>$data,'addresses'=>$address,'transactions'=>$transactions]);
	}
	public function updateClient(){
		$data=$this->input->post();
		/*print_r($data);
		exit;*/
		if($this->manager_model->update_client_info($data)){
			redirect('manager/allClients');
		}
		else{
			redirect('manager/allClients');
		}
	}
	public function updateSupplier(){
		$data=$this->input->post();
		/*print_r($data);
		exit;*/
		if($this->manager_model->update_client_info($data)){
			redirect('manager/allSupplier');
		}
		else{
			redirect('manager/allSupplier');
		}
	}
	public function addClientAddress(){
		$data=$this->input->post();
		$id=$data['client_info_clientID'];
		if($this->manager_model->add_client_address($data)){
			$this->session->set_flashdata('feedback_successfull', 'Added New Client Address Successfully');
			$data=$this->manager_model->get_client_details_by_ID($id);
			$address=$this->manager_model->get_client_address_details_by_ID($id);
			$transactions=$this->manager_model->get_client_transaction_details_by_ID($id);
			$this->load->view('manager/clientProfile',['data'=>$data,'addresses'=>$address,'transactions'=>$transactions]);
		}
		else{
			$this->session->set_flashdata('feedback_failed', 'Add New Client Address Failed!');
			$data=$this->manager_model->get_client_details_by_ID($id);
			$address=$this->manager_model->get_client_address_details_by_ID($id);
			$transactions=$this->manager_model->get_client_transaction_details_by_ID($id);
			$this->load->view('manager/clientProfile',['data'=>$data,'addresses'=>$address,'transactions'=>$transactions]);
		}
	}
	public function editAddressModal(){
		$data=$this->input->post();
		$id=$data['id'];
		unset($data['id']);
		//print_r($data); exit();
		if($this->manager_model->update_client_address($data)){
			$this->session->set_flashdata('feedback_successfull', 'Updated Client Address Successfully');
			//$data=$this->manager_model->get_client_details_by_ID($id);
			//$address=$this->manager_model->get_client_address_details_by_ID($id);
			//$transactions=$this->manager_model->get_client_transaction_details_by_ID($id);
			redirect("manager/clientProfile/{$id}");
		}
		else{
			$this->session->set_flashdata('feedback_failed', 'Updated Client Address Failed!');
			redirect("manager/clientProfile/{$id}");
		}
	}
	
	/* Start Bills*/
	public function addDebitNote(){
		$data=$this->manager_model->get_all_client_name_id();
		$this->load->view('manager/addDebitNote',['data'=>$data]);
	}
	public function storeDebitNote(){
		$data=$this->input->post();
		//print_r($data);exit;
		if($this->manager_model->add_debit_note($data)){
			$this->session->set_flashdata('feedback_successfull', 'Added New Debit Note Successfully');
			return redirect('manager/storeDebitNote');
		}
		else{
			$this->session->set_flashdata('feedback_failed', 'Add New Debit Note Failed!');
			return redirect('manager/storeDebitNote');
		}
	}
	public function addBill(){
		$data=$this->manager_model->get_all_supplier_name_id();
		$this->load->view('manager/addBill',['data1'=>$data]);
	}
	public function allBills(){
		$data=$this->manager_model->get_all_bill_info();
		$this->load->view('manager/allBills',['data'=>$data]);
	}
	public function viewBill($id){
		$data=$this->manager_model->get_bill_info_by_id($id);
		$data1=$this->manager_model->get_all_supplier_name_id();
		$this->load->view('manager/viewBill',['data'=>$data,'data1'=>$data1]);
	}
	public function storeBillInfo(){
		$data=$this->input->post();
		//print_r($data);exit;
		if($this->manager_model->add_bill_info($data)){
			$this->session->set_flashdata('feedback_successfull', 'Added New Bill Successfully');
			return redirect('manager/allBills');
		}
		else{
			$this->session->set_flashdata('feedback_failed', 'Add New Bill Failed!');
			return redirect('manager/addBill');
		}
	}
	public function updateBillInfo(){
	$data=$this->input->post();
	$admin_id=$data['billInfoId'];
	if($this->manager_model->update_bill_info($data)){
		$this->session->set_flashdata('feedback_successfull', 'Updated Bill Info Successfully');
		return redirect('manager/allBills');
	}
	else{
		$this->session->set_flashdata('feedback_failed', 'Update Bill Info Failed!');
		return redirect('manager/viewBill/'.$admin_id);
		}
	}
	/* End Bills */
	
	/* Start Client */
	public function allClients(){
		$data=$this->manager_model->get_all_clients();
		$this->load->view('manager/allClients',['data'=>$data]);
	}
	public function addClients(){
		$this->load->view('manager/addClients');
	}
	public function storeClient(){
		$data=$this->input->post();
		//print_r($data); exit;

		$this->load->library('form_validation');
		if ($this->form_validation->run('addClients') == FALSE){
			$this->session->set_flashdata('feedback_failed', 'Add New Client Failed!');
			return redirect('manager/addClients');
        }
		else{
			if($this->manager_model->addClient($data)){
				if($data['clientType']){
					$this->session->set_flashdata('feedback_successfull', 'Added New Supplier Successfully');
					return redirect('manager/allSupplier');
				}
				else{
					$this->session->set_flashdata('feedback_successfull', 'Added New Client Successfully');
					return redirect('manager/allClients');
				}
			}
			else{
				if($data['clientType']){
					$this->session->set_flashdata('feedback_failed', 'Add New Client Failed!');
					return redirect('manager/addSupplier');
				}
				else{
					$this->session->set_flashdata('feedback_failed', 'Add New Client Failed!');
					return redirect('manager/addClients');
				}
			}
		}
		//$this->load->view('manager/addClients');
	}
	public function allSupplier(){
		$data=$this->manager_model->get_all_suppliers();
		$this->load->view('manager/allSupplier',['data'=>$data]);
	}
	public function addSupplier(){
		$this->load->view('manager/addSupplier');
	}
	
	/*End Clients*/ 
	
	public function transactionReport(){
		$infos=$this->input->post();
		if($infos){
			if($infos['fromDate']){
				$infos['fromDate']= date('Y-m-d 00:00:00',strtotime($infos['fromDate']));
			}
			else{
				$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
				$infos['fromDate']= $dt->format('Y-m-d 00:00:00');
			}
			if($infos['toDate']){
				$infos['toDate']= date('Y-m-d 23:59:59',strtotime($infos['toDate']));
			}
			else{
				$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
				$infos['toDate']= $dt->format('Y-m-d 23:59:59');
			}
			//$data=$this->manager_model->get_all_transaction_info($infos);
		}
		else{
			$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
			$infos['fromDate']= $dt->format('Y-m-d 00:00:00');
			$infos['toDate']= $dt->format('Y-m-d 23:59:59');
			$infos['transectionType']=0;
			
		}
		$data=$this->manager_model->get_all_transaction_info($infos);
		//print_r($data);exit;
		
		//$this->load->view('manager/incomeExpenseReport');
		$name_id=$this->manager_model->get_all_transaction_type_name_id();
		$this->load->view('manager/transactionReport',['infos'=>$data,'data'=>$name_id,'dates'=>$infos]);
	}
	public function profitLossReport(){

		$this->load->view('manager/profitLossReport');
	}
	public function incomeExpenseReport(){
		$infos=$this->input->post();
		if($infos){
			if($infos['fromDate']){
				$infos['fromDate']= date('Y-m-d 00:00:00',strtotime($infos['fromDate']));
			}
			else{
				$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
				$infos['fromDate']= $dt->format('Y-m-d 00:00:00');
			}
			if($infos['toDate']){
				$infos['toDate']= date('Y-m-d 23:59:59',strtotime($infos['toDate']));
			}
			else{
				$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
				$infos['toDate']= $dt->format('Y-m-d 23:59:59');
			}
		}
		else{
			$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
			$infos['fromDate']= $dt->format('Y-m-d 00:00:00');
			$infos['toDate']= $dt->format('Y-m-d 23:59:59');
			$infos['transectionType']=0;
		}
		//print_r($infos);exit;
		$data=$this->manager_model->get_income_expense_report($infos);
		$this->load->view('manager/incomeExpenseReport',['infos'=>$data,'dates'=>$infos]);
	}
	
	public function saleReport(){

		$infos=$this->input->post();
	   if($infos){

	    	$data=$this->manager_model->get_all_sale_by_dates($infos);
	    }
	    else
	    {
	    	$data=$this->manager_model->get_all_sale_info();
	    }
	   
		//print_r($data); exit;
		//$infos=$this->manager_model->get_all_product_name_id();
		
		$this->load->view('manager/saleReport',['data'=>$data,'infos'=>$infos]);
	}
	
	public function productEntryReport(){
		$infos=$this->input->post();
	    if($infos){

	    	$data=$this->manager_model->get_all_product_update_by_dates($infos);
	    }
	    else
	    {
	    	$data=$this->manager_model->get_all_product_update();
	    }
		//print_r($data); exit;
		$infos=$this->manager_model->get_all_product_name_id();
		$this->load->view('manager/productEntryReport',['data'=>$data,'infos'=>$infos]);
	}
	/* Users */
	public function allUser(){
		$data=$this->manager_model->get_all_user();
		$this->load->view('manager/allUser',['infos'=>$data]);
	}
	public function addUser(){
		$this->load->view('manager/addUser');
	}
	public function storeUser(){
		
		$this->load->library('form_validation');
		if ($this->form_validation->run('add_user') == FALSE){
			$this->load->view('manager/addUser');
        }
		else{
			$data=$this->input->post();
			if($this->manager_model->add_user($data)){
				$this->session->set_flashdata('feedback_successfull', 'Added New User Successfully');
				return redirect('manager/allUser');
			}
			else{
				$this->session->set_flashdata('feedback_failed', 'Add New User Failed!');
				return redirect('manager/addUser');
			}
		}
	}
	public function updateAdminInfo(){
		

		$this->load->library('form_validation');
		if ($this->form_validation->run('updateAdmin') == FALSE){
			//$this->session->set_flashdata('feedback_failed', 'Update Admin Info Failed!');
			return redirect('manager/myAccount');
        }
		else{
			$data=$this->input->post();
			
			$pass=$this->manager_model->get_password_by_id($data['adminID']);
			//print_r($pass); 
			//echo "   ";
			//print_r($data['adminPassword']); exit();
			if($data['adminPassword']==$pass){
				
			
				if($this->manager_model->update_admin_info($data)){
					//print_r($data);exit();
					redirect('login/logout');
				}
				else{
					$this->session->set_flashdata('feedback_failed', 'Update Admin Info Failed!');
					redirect('manager/myAccount');
				}
		  	}
		  else{
		  	$this->session->set_flashdata('feedback_failed', 'Update Admin Info Failed!');
				redirect('manager/myAccount');
			}
		}
	}
	
	public function viewUser($admin_id){
		$data=$this->manager_model->get_user_details_by_id($admin_id);
		$this->load->view('manager/viewUser',['info'=>$data]);
	}
	public function updateUser(){
		$data=$this->input->post();
		$admin_id=$data['adminID'];
		if($this->manager_model->update_user($data,$admin_id)){
			$this->session->set_flashdata('feedback_successfull', 'Updated User Successfully');
			return redirect('manager/allUser');
		}
		else{
				$this->session->set_flashdata('feedback_failed', 'Update User Failed!');
				return redirect('manager/viewUser/'.$admin_id);
			}
		//$data=$this->manager_model->get_user_details_by_id($data['adminID']);
		//$this->load->view('manager/viewUser',['info'=>$data]);
	}
	public function updateProductInfo(){
		$data=$this->input->post();
		$admin_id=$data['productId'];
		if($this->manager_model->update_product_info($data)){
			$this->session->set_flashdata('feedback_successfull', 'Updated Product Info Successfully');
			return redirect('manager/showInventory');
		}
		else{
				$this->session->set_flashdata('feedback_failed', 'Update Product Info Failed!');
				return redirect('manager/productDetails/'.$admin_id);
			}
		//$data=$this->manager_model->get_user_details_by_id($data['adminID']);
		//$this->load->view('manager/viewUser',['info'=>$data]);
	}

  public function storeCashAccount(){
		$data=$this->input->post();
		if($this->manager_model->add_cash_account($data)){
			$this->session->set_flashdata('feedback_successfull', 'Added New Cash Account Successfully');
			return redirect('manager/allCashAccount');
		}
		else{
			$this->session->set_flashdata('feedback_failed', 'Add New Cash Account Failed!');
			return redirect('manager/addCashAccountModal');
		}
		//$this->load->view('manager/addNewProductToInventory');
	}
	public function updateCashAccount(){
		$data=$this->input->post();
		$admin_id=$data['cashAccountInfoId'];
		if($this->manager_model->update_Cash_Account($data)){
			$this->session->set_flashdata('feedback_successfull', 'Updated Cash Account Successfully');
			return redirect('manager/allCashAccount');
		}
		else{
				$this->session->set_flashdata('feedback_failed', 'Update Cash Account Failed!');
				return redirect('manager/viewCashAccount/'.$admin_id);
			}
		//$data=$this->manager_model->get_user_details_by_id($data['adminID']);
		//$this->load->view('manager/viewUser',['info'=>$data]);
	}
	/* Start Petty Cash*/
	public function pettyCash(){
		$data=$this->manager_model->get_all_petty_cash_info();
		$amount=$this->manager_model->get_total_petty_cash_amount();
		//print_r($amount);exit;
		$this->load->view('manager/pettyCash',['data'=>$data,'totalAmount'=>$amount]);
	}
	/* End Petty Cash*/
	public function removeAddress($id)
	{
	   $this->manager_model->remove_address($id);
	    	redirect("manager/allClients");
	    
		
	}
}
